package sk.tekel04.scorched.game;

import java.awt.Point;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.Model;
import sk.tekel04.scorched.picture.SelectablePicture;
import sk.tekel04.scorched.weapon.UserMissileFactory;

/**
 * Objekt reprezentujuci jeden tank v hre. Tank patri konkretnemu hracovi, a ma prislusnu farbu tohto hraca. Po
 * vytvoreni nie je mozne tankom hybat po krajine. Kazdy tank ma jedinecne identifikacne cislo, na zaklade ktoreho je
 * mozne rozoznat, ktory tank si pouzivatel vybral. Kazdy tank dokaze vystrelit lubovolny typ strely.
 */
public class Tank implements SelectablePicture {

	/** Maximalny mozny pocet hracov v hre. */
	public static final int MAX_PLAYERS = 8;

	/** Mena jednotlivych hracov. */
	public static final String[] PLAYER_NAME = { "Red", "Blue", "Yellow", "Purple", "Green", "Teal", "Orange", "Tan" };

	/** Farba tankov jednotlivych hracov. */
	public static final float[][] PLAYER_COLOR = { //
			{ 255 / 255f, 0 / 255f, 0 / 255f }, // Red
			{ 49 / 255f, 82 / 255f, 255 / 255f }, // Blue
			{ 255 / 255f, 255 / 255f, 0 / 255f }, // Yellow
			{ 140 / 255f, 41 / 255f, 165 / 255f }, // Purple
			{ 128 / 255f, 255 / 255f, 0 / 255f }, // Green
			{ 8 / 255f, 156 / 255f, 165 / 255f }, // Teal
			{ 255 / 255f, 132 / 255f, 0 / 255f }, // Orange
			{ 125 / 255f, 92 / 255f, 66 / 255f } }; // Tan

	/** Objekt na urcovanie vysky krajiny v danom bode. */
	private static Land.LandMeter landMeter;

	/** Model spodnej casti tanku, nikdy sa neotaca. */
	private static Model model_bottom;

	/** Model vrchnej casti tanku, otaca sa podla natocenia tanku. */
	private static Model model_top;

	/** Model kanonu, otaca sa podla zdvihnutia kanonu. */
	private static Model model_cannon;

	/** Cislo hraca, ktoremu patri tento tank. */
	public final int player;

	/**
	 * Identifikator tanku, jedinecny pre kazdy tank. Umoznuje urcit, ktory tank bol vybrany pouzivatelom.
	 *
	 * @see GraphicsListener#mouseSelection(GL, Point)
	 */
	public final int id;

	/** Pozicia v krajine. */
	private final double x;

	/** Pozicia v krajine. */
	private final double y;

	/** Otocenie tanku. */
	private double direction;

	/** Uhol zdvihnutia kanonu. */
	private double angle;

	/** Sila vystrelu. */
	private double power;

	/** Zvolena zbran. */
	private int weapon;

	/** Pocet zivotov tanku. */
	private double life = 100;

	/** Vyska, v ktorej sa nachadza tank. */
	private double height = 1;

	/**
	 * Konstruktor tanku nastavi jeho identifikacne cislo, hraca, ktoremu patri, a poziciu v krajine.
	 *
	 * @param id identifikacne cislo tanku
	 * @param player cislo hraca, ktoremu tank patri
	 * @param x x-suradnica v krajine
	 * @param y y-suradnica v krajine
	 */
	public Tank(int id, int player, int x, int y) {
		this.id = id;
		this.player = player;
		this.x = x + 0.5;
		this.y = y + 0.5;
	}

	/**
	 * Nakresli tank. Pred vykreslenim tanku bude cele policko krajiny, na ktorom sa nachadza, zarovnane, takze tank
	 * bude na vodorovnej ploche. Tank bude prislusnej farby podla toho, akemu hracovi patri. Vrchna cast tanku a hlaven
	 * dela budu otocene tak, aby obrazok odrazal stav tanku.
	 *
	 * @see Land.LandMeter#makePlatform(int, int)
	 */
	@Override
	public void draw(GL2 gl, boolean select) {
		if (select) {
			gl.glLoadName(id);
		}

		gl.glPushMatrix();

		gl.glDisable(GL2.GL_LIGHTING);
		gl.glColor3fv(PLAYER_COLOR[player], 0);

		height = landMeter.makePlatform((int) x, (int) y);
		gl.glTranslated(x, height, y);
		model_bottom.draw(gl);

		gl.glRotated(direction, 0, -1, 0);
		model_top.draw(gl);

		gl.glTranslated(0.05, 0.323, 0.0);
		gl.glRotated(angle, 0, 0, 1);
		model_cannon.draw(gl);

		gl.glColor3d(1, 1, 1);
		gl.glEnable(GL2.GL_LIGHTING);

		gl.glPopMatrix();
	}

	/**
	 * Nastavi nove otocenie tanku (sever, juh ...).
	 *
	 * @param direction otocenie tanku
	 * @uml.property name="direction"
	 */
	public void setDirection(double direction) {
		this.direction = direction;
	}

	/**
	 * Vrati aktualne otocenie tanku (sever, juh...).
	 *
	 * @uml.property name="direction"
	 */
	public double getDirection() {
		return direction;
	}

	/**
	 * Nastavi novy uhol kanonu (vodorovne, nahor...).
	 *
	 * @uml.property name="angle"
	 */
	public void setAngle(double angle) {
		this.angle = angle;
	}

	/**
	 * Vrati aktualny uhol kanonu (vodorovne, nahor...)..
	 *
	 * @uml.property name="angle"
	 */
	public double getAngle() {
		return angle;
	}

	/**
	 * Nastavi novu silu vystrelu.
	 *
	 * @uml.property name="power"
	 */
	public void setPower(double power) {
		this.power = power;
	}

	/**
	 * Vrati nastavenu silu vystrelu.
	 *
	 * @uml.property name="power"
	 */
	public double getPower() {
		return power;
	}

	/**
	 * Nastavi iny typ zbrane. Tato metoda nema ziaden vplyv na to, aku zbran nakoniec tank vystreli, ma vyznam len pre
	 * vypis v hernom menu.
	 *
	 * @param weapon cislo novej zbrane
	 * @uml.property name="weapon"
	 */
	public void setWeapon(int weapon) {
		this.weapon = weapon;
	}

	/**
	 * Vrati cislo zvolenej zbrane.
	 *
	 * @uml.property name="weapon"
	 */
	public int getWeapon() {
		return weapon;
	}

	/**
	 * Vrati zivoty tanku. Vrati -1 pre tank pod vodou.
	 *
	 * @uml.property name="life"
	 */
	public double getLife() {
		return (height > 0) ? life : -1;
	}

	/**
	 * Vytvori novu strelu na konci dela tanku. Strela bude mat smerovanie a silu podla aktualneho nastavenia tanku.
	 *
	 * @param missileFactory Factory trieda definujuca typ strely
	 */
	public void shoot(UserMissileFactory missileFactory) {
		double cannon = 0.45 * Math.cos(angle * Math.PI / 180);

		// Vypocet pozicie konca kanonu
		double dx = cannon * Math.cos(direction * Math.PI / 180);
		double dy = 0.45 * Math.sin(angle * Math.PI / 180);
		double dz = cannon * Math.sin(direction * Math.PI / 180);

		missileFactory.createInstance(x + dx, landMeter.height(x, y) + 0.323 + dy, y + dz, direction, angle,
				power / 80);
	}

	/**
	 * Sposobi zranenia tomuto tanku, ak sa epicentrum guloveho vybuchu nachadza na danom mieste krajiny. Sposobene
	 * zranenia klesaju umerne od epicentra, az od danej vzdialenosti nesposobuju ziadnu skodu. Tato metoda nesposobi
	 * ziadne graficke znazornenie vybuchu, ani nevytvori ziadne poskodenie krajiny.
	 *
	 * @param x x-suradnica epicentra vybuchu
	 * @param y y-suradnica epicentra vybuchu
	 * @param z z-suradnica epicentra vybuchu
	 * @param radius polomer vybuchu, maximalna vzdialenost, na ktoru tento vybuch sposobuje zranenia
	 * @param damage zranenia, ktore sposobi vybuch presne v epicentre
	 */
	public void damage(double x, double y, double z, double radius, double damage) {
		if (radius <= 0 || damage <= 0) {
			return;
		}

		double dx = this.x - x;
		double dy = this.y - z;
		double dz = landMeter.height(this.x, this.y) - y;
		double dist = Math.sqrt(dx * dx + dy * dy + dz * dz);
		if (dist < radius) {
			life -= damage * Math.pow(1 - dist / radius, 1.5);
		}
	}

	/**
	 * Nastavi objekt na urcovanie vysky krajiny.
	 *
	 * @param landMeter
	 * @uml.property name="landMeter"
	 */
	public static void setLandMeter(Land.LandMeter landMeter) {
		Tank.landMeter = landMeter;
	}

	/** Nacita vsetky modely prisluchajuce tanku. */
	public static void loadModels(GL2 gl) {
		model_bottom = new Model(gl, "tank.png", "TankBottom");
		model_top = new Model(gl, "tank.png", "TankTop");
		model_cannon = new Model(gl, "tank.png", "TankCannon");
	}

}
