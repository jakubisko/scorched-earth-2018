package sk.tekel04.scorched.game;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.SimpleTextureLoader;
import sk.tekel04.scorched.picture.UnselectablePicture;

/** Kresli oblohu. */
public class Skybox implements UnselectablePicture {

	/**
	 * Na hranach vrchnej steny kocky bolo vidiet tenke jednopixelove ciary. Boli to pixely z obvodu textury. Umyselne
	 * skratime texturu kazdej plochy na okraji o pol pixelu, aby sa nezobrazovala.
	 */
	private static final double PIXEL_X = 0.5 / 4096; // Sirka skybox textury
	private static final double PIXEL_Y = 0.5 / 3072; // Vyska skybox textury

	/** Textura oblohy. */
	private static int texture_skybox;

	/** Nakresli oblohu. */
	@Override
	public void draw(GL2 gl) {
		gl.glPushMatrix();

		// Obloha sa hybe s kamerou.
		gl.glTranslated(MovementListener.cameraX, MovementListener.cameraY, MovementListener.cameraZ);
		gl.glRotated(170, 0, 1, 0); // Pri tomto otoceni poloha slnka na skyboxe zodpoveda tienom na kopcoch
		gl.glScaled(50, 50, 50);

		gl.glDisable(GL2.GL_DEPTH_TEST);
		gl.glDisable(GL2.GL_LIGHTING);

		gl.glBindTexture(GL2.GL_TEXTURE_2D, texture_skybox);

		drawBox(gl);

		gl.glEnable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_DEPTH_TEST);

		gl.glPopMatrix();
	}

	/** Nakresli otexturovanu kocku. */
	private void drawBox(GL2 gl) {
		gl.glBegin(GL2.GL_QUADS);

		// Front Face
		gl.glNormal3d(0, 0, 1);
		gl.glTexCoord2d(0.25, 1.0 / 3 + PIXEL_Y);
		gl.glVertex3d(-1, -1, 1); // Bottom Left Of The Texture and Quad
		gl.glTexCoord2d(0.5, 1.0 / 3 + PIXEL_Y);
		gl.glVertex3d(1, -1, 1); // Bottom Right Of The Texture and Quad
		gl.glTexCoord2d(0.5, 2.0 / 3 - PIXEL_Y);
		gl.glVertex3d(1, 1, 1); // Top Right Of The Texture and Quad
		gl.glTexCoord2d(0.25, 2.0 / 3 - PIXEL_Y);
		gl.glVertex3d(-1, 1, 1); // Top Left Of The Texture and Quad

		// Back Face
		gl.glNormal3d(0, 0, -1);
		gl.glTexCoord2d(1, 1.0 / 3 + PIXEL_Y);
		gl.glVertex3d(-1, -1, -1); // Bottom Right Of The Texture and Quad
		gl.glTexCoord2d(1, 2.0 / 3 - PIXEL_Y);
		gl.glVertex3d(-1, 1, -1); // Top Right Of The Texture and Quad
		gl.glTexCoord2d(0.75, 2.0 / 3 - PIXEL_Y);
		gl.glVertex3d(1, 1, -1); // Top Left Of The Texture and Quad
		gl.glTexCoord2d(0.75, 1.0 / 3 + PIXEL_Y);
		gl.glVertex3d(1, -1, -1); // Bottom Left Of The Texture and Quad

		// Top Face
		gl.glNormal3d(0, 1, 0);
		gl.glTexCoord2d(0.25 + PIXEL_X, 1 - PIXEL_Y);
		gl.glVertex3d(-1, 1, -1); // Top Left Of The Texture and Quad
		gl.glTexCoord2d(0.25 + PIXEL_X, 2.0 / 3);
		gl.glVertex3d(-1, 1, 1); // Bottom Left Of The Texture and Quad
		gl.glTexCoord2d(0.5 - PIXEL_X, 2.0 / 3);
		gl.glVertex3d(1, 1, 1); // Bottom Right Of The Texture and Quad
		gl.glTexCoord2d(0.5 - PIXEL_X, 1 - PIXEL_Y);
		gl.glVertex3d(1, 1, -1); // Top Right Of The Texture and Quad

		// Right face
		gl.glNormal3d(1, 0, 0);
		gl.glTexCoord2d(0.75, 1.0 / 3 + PIXEL_Y);
		gl.glVertex3d(1, -1, -1); // Bottom Right Of The Texture and Quad
		gl.glTexCoord2d(0.75, 2.0 / 3 - PIXEL_Y);
		gl.glVertex3d(1, 1, -1); // Top Right Of The Texture and Quad
		gl.glTexCoord2d(0.5, 2.0 / 3 - PIXEL_Y);
		gl.glVertex3d(1, 1, 1); // Top Left Of The Texture and Quad
		gl.glTexCoord2d(0.5, 1.0 / 3 + PIXEL_Y);
		gl.glVertex3d(1, -1, 1); // Bottom Left Of The Texture and Quad

		// Left Face
		gl.glNormal3d(-1, 0, 0);
		gl.glTexCoord2d(0, 1.0 / 3 + PIXEL_Y);
		gl.glVertex3d(-1, -1, -1); // Bottom Left Of The Texture and Quad
		gl.glTexCoord2d(0.25, 1.0 / 3 + PIXEL_Y);
		gl.glVertex3d(-1, -1, 1); // Bottom Right Of The Texture and Quad
		gl.glTexCoord2d(0.25, 2.0 / 3 - PIXEL_Y);
		gl.glVertex3d(-1, 1, 1); // Top Right Of The Texture and Quad
		gl.glTexCoord2d(0, 2.0 / 3 - PIXEL_Y);
		gl.glVertex3d(-1, 1, -1); // Top Left Of The Texture and Quad

		// Bottom Face // Our application doesn't draw bottom face
		// gl.glNormal3d(0, -1, 0);
		// gl.glTexCoord2d(0.5 - PIXEL_X, PIXEL_Y);
		// gl.glVertex3d(-1, -1, -1); // Top Right Of The Texture and Quad
		// gl.glTexCoord2d(0.25 + PIXEL_X, PIXEL_Y);
		// gl.glVertex3d(1, -1, -1); // Top Left Of The Texture and Quad
		// gl.glTexCoord2d(0.25 + PIXEL_X, 1.0 / 3);
		// gl.glVertex3d(1, -1, 1); // Bottom Left Of The Texture and Quad
		// gl.glTexCoord2d(0.5 - PIXEL_X, 1.0 / 3);
		// gl.glVertex3d(-1, -1, 1); // Bottom Right Of The Texture and Quad

		gl.glEnd();
	}

	/** Nacita texturu oblohy. */
	public static void loadTexture(GL2 gl) {
		SimpleTextureLoader loader = new SimpleTextureLoader();
		texture_skybox = loader.loadTexture(gl, "skybox.jpg");
	}

}
