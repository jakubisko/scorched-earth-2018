package sk.tekel04.scorched.game;

import java.awt.Color;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.game.Land.LandMeter;
import sk.tekel04.scorched.logging.Log;
import sk.tekel04.scorched.picture.SelectablePicture;
import sk.tekel04.scorched.picture.SphereDrawer;
import sk.tekel04.scorched.weapon.AbstractMissile;
import sk.tekel04.scorched.weapon.AbstractWeapon;
import sk.tekel04.scorched.weapon.Explosion;
import sk.tekel04.scorched.weapon.UserMissileFactory;
import sk.tekel04.scorched.weapon.WeaponList;
import sk.tekel04.scorched.windows.FrameOpenGL;
import sk.tekel04.scorched.windows.GameControlsDialog;

/**
 * Obsahuje vsetky herne objekty - krajinu, tanky, zbrane atd. Cez tento objekt sa da riadit hra z inych okien. Pri
 * vytvoreni automaticky vytvori okno s grafikou {@link FrameOpenGL} pre tuto hru, a objekt {@link Land} ,
 * reprezentujuci krajinu.
 */
public class Game implements SelectablePicture {

	/**
	 * Frames per second - frekvencia obnovy obrazu. Frekvencia obnovy nepresiahne tuto hodnotu. Na pomalsich pocitacoch
	 * sa obraz obnovuje najvyssou moznou rychlostou.
	 */
	public static final int FPS = 60;

	/**
	 * Herny cas, ktory uplynie behom 1 framu. Tento cas treba pouzivat vo vsetkych fyzikalnych vzorcoch. V pripade
	 * zvysenia FPS sa prislusne znizi cas na frame. Kedze fyzika sa aplikuje v kazdom frame, zvysenie FPS pri
	 * konstantnej rychlosti fyziky by zvysilo aj rychlost hry. (16 FPS bola povodna rychlost hry. Toto cislo nema
	 * ziadny vyznam.)
	 */
	public static final double TIME = 16.0 / FPS;

	/** Herne menu. */
	private GameControlsDialog gameControlsDialog;

	/** Pevnina. */
	private Land land = new Land();

	/** Objekt na urcovanie vysky v danom bode krajiny. */
	private final Land.LandMeter landMeter = land.new LandMeter();

	/** Vodna hladina. */
	private Water water = new Water();

	/** Obloha. */
	private Skybox skybox = new Skybox();

	/** Zoznam aktualne letiacich zbrani. */
	private WeaponList weaponList = new WeaponList();

	/** Zoznam vsetkych tankov. */
	private List<Tank> tanks = new ArrayList<>();

	/** Tank naposledy zvoleny pouzivatelom. */
	private Tank selectedTank;

	/** Hodnota urcujuca, ci tato hra prebieha, alebo je momentalne zapnute menu. */
	private boolean gameStopped = true;

	/** Cislo hraca, ktory je prave na tahu. */
	private int turn;

	/** Metoda, ktoru zavolat na konci hry. Textovy argument opisujuci vitaza sa zobrazi pouzivatelovi. */
	private Consumer<String> endGameWithMessage;

	/**
	 * Inicializuje triedu Game. Nastavi ostatnym objektom potrebne parametre. Triedam nachadzajucim sa v krajine + *
	 * nastavi referenciu na merac vysky, zoznam zbrani a zoznam tankov.
	 */
	public void init(GameControlsDialog gameControlsDialog, Consumer<String> endGameWithMessage) {
		Log.entering("Game", "init");

		this.gameControlsDialog = gameControlsDialog;
		this.endGameWithMessage = endGameWithMessage;

		Tank.setLandMeter(landMeter);
		AbstractWeapon.setLandMeter(landMeter);
		AbstractWeapon.setWeaponList(weaponList);
		Explosion.setTanky(tanks);

		Log.exiting("Game", "init");
	}

	/** Nacita vsetky textury a modely v hre. */
	public static void loadTexturesAndModels(GL2 gl) {
		Log.entering("Game", "loadTexture");

		Land.loadTexture(gl);
		Water.loadTexture(gl);
		Skybox.loadTexture(gl);
		Tank.loadModels(gl);
		AbstractMissile.loadModels(gl);
		SphereDrawer.loadQuadric(gl);

		Log.exiting("Game", "loadTexture");
	}

	/**
	 * Vrati {@link LandMeter} prisluchajuci k mape tejto hry
	 *
	 * @uml.property name="landMeter"
	 */
	public Land.LandMeter getLandMeter() {
		return landMeter;
	}

	/**
	 * Vykona jeden krok (jeden obrazok) hry. Ak je pocas tohto kroku znicena posledna letiaca zbran v hre, dostane sa
	 * na tah novy hrac. Zaroven obnovi vsetky udaje pouzivatelom naposledy oznacenemu tanku. Kedze kazdy objekt sa
	 * posunie prave raz, a kazdy objekt sa nakresli prave raz, rychlost kreslenia neovplyvni drahu letiacich striel.
	 */
	public void gameStep(GL2 gl) {
		if (gameStopped) {
			return;
		}

		try {
			updateSelectedTank();

			boolean wasEmpty = weaponList.isEmpty();

			draw(gl, false);

			if (!wasEmpty && weaponList.isEmpty()) {
				newTurn();
			}
		} catch (Throwable t) {
			Log.exception("Failed to execute game step", t);
			throw t;
		}
	}

	/**
	 * Nakresli obrazok hry. Postupne nakresli vsetky zbrane, tanky a krajinu. Zaroven posunie vsetky zbrane o jeden
	 * krok. Vsetky tanky a vsetky zbrane, ktore boli znicene, sa odstrania z prislusnych zoznamov.
	 */
	@Override
	public void draw(GL2 gl, boolean select) {
		if (gameStopped) {
			return;
		}

		// Oznacit sa daju len tanky. Krajina ich vie prekryt. Zvysne objekty, napr. strely, sa neberu do uvahy.
		if (!select) {
			skybox.draw(gl);
			weaponList.draw(gl);
			water.draw(gl);
		}

		for (int id = 0; id < tanks.size(); id++) {
			Tank tank = tanks.get(id);
			if (tank != null) {
				if (tank.getLife() <= 0) {
					tanks.set(id, null);
					Log.info("Tank destroyed");
				} else {
					tank.draw(gl, select);
				}
			}
		}

		land.draw(gl, select);
	}

	/**
	 * Vytvori novu hru na aktualnej mape. Vsetky objekty, ktore zostali z predoslej hry, su znicene. Na krajine sa
	 * vytvori zadany pocet novych tankov, a nahodne sa urci, ktory hrac je na tahu. Ak je krajina prilis mala pre dany
	 * pocet tankov, alebo ak ziadna krajina nebola vytvorena, tato metoda vrati chybu.
	 *
	 * @param numberOfPlayers pocet hracov v novej hre
	 * @param numberOfTanks   pocet tankov pre kazdeho hraca
	 * @throws WrongGameSettingsException sprava pre pouzivatela oznacujuca problem s danou krajinou
	 */
	public void newGame(int numberOfPlayers, int numberOfTanks) throws WrongGameSettingsException {
		tanks.clear();
		weaponList.clear();

		land.placeTanks(tanks, numberOfPlayers, numberOfTanks);

		turn = (int) (numberOfPlayers * Math.random());
		newTurn();
	}

	/**
	 * Nacita krajinu zo suboru.
	 *
	 * @param fileWithLand subor, z ktoreho sa nacita nova krajina
	 */
	public void loadLandFromFile(Path fileWithLand) throws IOException {
		land.loadFromFile(fileWithLand);
	}

	/**
	 * Vytvori nahodnu krajinu s danymi parametrami.
	 *
	 * @param size  rozmer novej mapy
	 * @param hills vyska kopcov
	 * @param water povrch krajiny pokryty vodou v percentach
	 */
	public void createRandomLand(int size, int hills, int water) {
		try {
			land.createRandom(size, hills, water);
		} catch (Exception ex) {
			Log.exception("Failed to create random land", ex);
		}
	}

	/**
	 * Vyprazdni aktualnu krajinu. Na takejto krajine nie je mozne odohrat hru. Pouzivatel bude nuteny nacitat pred
	 * dalsou hrou novu mapu.
	 */
	public void clearLand() {
		land.empty();
	}

	/**
	 * Oznaci dany tank a zobrazi jeho stav v menu hry. Ak dany tank neexistuje, nadalej zostava zobrazeny stav
	 * naposledy oznaceneho tanku.
	 *
	 * @param id identifikacne cislo tanku, ktory sa ma oznacit
	 */
	public void selectTank(int id) {
		if (id >= 0 && id < tanks.size()) {
			selectedTank = tanks.get(id);
		}

		if (selectedTank != null) {
			gameControlsDialog.setValues(selectedTank.getDirection(), selectedTank.getAngle(), selectedTank.getPower(),
					selectedTank.getLife(), selectedTank.getWeapon());
			gameControlsDialog.setEnabled((selectedTank.player == turn) && weaponList.isEmpty());
		}
	}

	/**
	 * Obnovi stav oznaceneho tanku podla hodnot nastavenych v menu hry. Ak ziaden tank nie je oznaceny, neurobi nic.
	 */
	private void updateSelectedTank() {
		if (selectedTank == null) {
			return;
		}

		selectedTank.setDirection(gameControlsDialog.getDirection());
		selectedTank.setAngle(gameControlsDialog.getAngle());
		selectedTank.setPower(gameControlsDialog.getPower());
		selectedTank.setWeapon(gameControlsDialog.getWeapon());
	}

	/**
	 * Oznaceny tank vystreli dany typ strely. Ak nie je zvoleny ziaden tank, alebo ak ziaden hrac nie je na tahu, nic
	 * sa nestane. Zaroven sa vypne moznost menit udaje v menu hry az do zaciatku dalsieho tahu.
	 *
	 * @param missileFactory Factory trieda definujuca typ strely
	 */
	public void selectedTankShoot(UserMissileFactory missileFactory) {
		if (!weaponList.isEmpty() || selectedTank == null) {
			return;
		}

		updateSelectedTank();
		selectedTank.shoot(missileFactory);

		gameControlsDialog.setEnabled(false);
	}

	/**
	 * Posunie tah dalsiemu hracovi a obnovi text oznacujuci, ktory hrac je na tahu. Ak v hre nie su aspon 2 hraci, hra
	 * sa ukonci.
	 */
	private void newTurn() {
		// Zistime, kolko tankov ma kazdy hrac
		int[] players = new int[Tank.MAX_PLAYERS];
		Arrays.fill(players, 0);

		for (Tank tank : tanks) {
			if (tank != null && tank.getLife() > 0) {
				players[tank.player]++;
			}
		}

		// Zistime, kolko hracov ma aspon 1 tank
		int remains = 0;
		for (int i = 0; i < Tank.MAX_PLAYERS; i++) {
			if (players[i] > 0) {
				remains++;
			}
		}

		// V hre zostalo 0 hracov
		if (remains == 0) {
			endGameWithMessage.accept("All players have been destroyed");
			return;
		}

		// Posunieme tah dalsiemu hracovi
		do {
			turn = (turn + 1) % Tank.MAX_PLAYERS;
		} while (players[turn] == 0);

		// V hre zostal 1 hrac, koniec
		if (remains == 1) {
			endGameWithMessage.accept(Tank.PLAYER_NAME[turn] + " player wins");
			return;
		}

		// V hre zostali aspon 2 hraci, napiseme, kto je na tahu
		gameControlsDialog.setPlayer(Tank.PLAYER_NAME[turn],
				new Color(Tank.PLAYER_COLOR[turn][0], Tank.PLAYER_COLOR[turn][1], Tank.PLAYER_COLOR[turn][2]));

		Log.info("Turn passed to player " + Tank.PLAYER_NAME[turn]);
	}

	public void setGameStopped(boolean gameStopped) {
		this.gameStopped = gameStopped;
	}

}
