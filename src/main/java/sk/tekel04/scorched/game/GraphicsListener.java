package sk.tekel04.scorched.game;

import java.awt.Point;
import java.nio.IntBuffer;
import java.util.function.IntConsumer;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;

import sk.tekel04.scorched.logging.Log;

/**
 * Implementuje rozhranie GLEventListener, vdaka comu ma na starosti obsluhu grafiky. Tato trieda sa stara o vykonavanie
 * nizkych prikazov, samotne kreslenie hry prebieha v triede {@link Game} . Pouzivatelsky vstup je spracovany v triede
 * {@link MovementListener} .
 */
public class GraphicsListener implements GLEventListener {

	/** OpenGL Utility Library, poskytuje vyssie prikazy na pracu s OpenGL */
	private final GLU glu = new GLU();

	/** Objekt spracuvajuci pouzivatelske vstupy. */
	private final MovementListener movementListener;

	/** Hra, ktorej patri tato grafika. */
	private final Game game;

	/** Objekt starajuci sa o pravidelne prekreslovanie. */
	private FPSAnimator animator;

	/** Pomer sirky a vysky okna. */
	private double proportion;

	/** Objekt zobrazujuci pocet zobrazeni za sekundu. */
	private final IntConsumer fpsDisplay;

	/** Used for calculation of actual FPS. */
	private long startOfMeasurement = System.currentTimeMillis();

	/** Used for calculation of actual FPS. */
	private int framesPassed = 0;

	public GraphicsListener(MovementListener movementListener, Game game, IntConsumer fpsDisplay) {
		this.movementListener = movementListener;
		this.game = game;
		this.fpsDisplay = fpsDisplay;
	}

	/** Inicializacia grafiky - osvetlenie, nacitanie textur a modelov, nastavenie FPS. */
	@Override
	public void init(GLAutoDrawable glDrawable) {
		Log.entering("GraphicsListener", "init");

		GL2 gl = glDrawable.getGL().getGL2();

		Game.loadTexturesAndModels(gl);

		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glClearDepth(1.0);
		gl.glDepthFunc(GL2.GL_LEQUAL);
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glShadeModel(GL2.GL_SMOOTH);
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);

		float[] lightAmbient = { 0.7f, 0.7f, 0.7f, 1.0f };
		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, lightAmbient, 0);

		float[] lightDiffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, lightDiffuse, 0);

		gl.glEnable(GL2.GL_LIGHT1);
		gl.glEnable(GL2.GL_LIGHTING);

		animator = new FPSAnimator(glDrawable, Game.FPS);
		animator.start();

		Log.exiting("GraphicsListener", "init");
	}

	/**
	 * Identifikacia oznaceneho objektu. Tato metoda prejde vsetky nakreslene objekty, a zisti, ktory z nich bol
	 * oznaceny pouzivatelom. Ak sa na danom mieste obrazovky nachadza viacero objektov, vysledkom je objekt najblizsie
	 * k obrazovke.
	 *
	 * @param point pouzivatelom vybrany bod na obrazovke
	 * @return identifikator oznaceneho objektu, alebo -1, ak nebol oznaceny ziaden objekt
	 */
	private int findSelected(GL2 gl, Point point) {
		int[] viewport = new int[4];
		gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);

		IntBuffer selectBuffer = Buffers.newDirectIntBuffer(512);
		gl.glSelectBuffer(512, selectBuffer);

		gl.glRenderMode(GL2.GL_SELECT);

		gl.glInitNames();
		gl.glPushName(-1);

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glPushMatrix();
		gl.glLoadIdentity();
		glu.gluPickMatrix(point.x, viewport[3] - point.y, 1.0, 1.0, viewport, 0);

		glu.gluPerspective(45.0, proportion, 0.1, 160.0);
		gl.glMatrixMode(GL2.GL_MODELVIEW);

		// nakreslime objekty aj s ich identifikatormi
		game.draw(gl, true);

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glPopMatrix();

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		int hits = gl.glRenderMode(GL2.GL_RENDER);

		int selectedObject = -1;

		// zistime, ktory objekt je najblizsie k obrazovke
		if (hits > 0) {
			double depth = selectBuffer.get(1);
			selectedObject = selectBuffer.get(3);
			for (int i = 1; i < hits; i++) {
				if (selectBuffer.get(4 * i + 1) < depth) {
					depth = selectBuffer.get(4 * i + 1);
					selectedObject = selectBuffer.get(4 * i + 3);
				}
			}
		}

		return selectedObject;
	}

	/**
	 * Posunie kameru podla pouzivatelskeho vstupu, a nakresli vsetky objekty. Ak pouzivatel klikol mysou, identifikuje
	 * sa oznaceny objekt.
	 *
	 * @see MovementListener#moveScreen(GL)
	 */
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		movementListener.moveScreen(gl);

		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		float[] lightPosition = { 1.0f, -3.0f, 1.0f, 0.0f };
		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, lightPosition, 0);

		game.gameStep(gl);

		Point point = movementListener.getMousePoint();
		if (point != null) {
			int selectedTankId = findSelected(gl, point);
			game.selectTank(selectedTankId);
		}

		gl.glFlush();

		displayFPS();
	}

	/** Measures and outputs actual Frames Per Second */
	private void displayFPS() {
		framesPassed++;

		final long now = System.currentTimeMillis();

		if (now - startOfMeasurement > 1000) {
			fpsDisplay.accept(framesPassed);
			framesPassed = 0;
			startOfMeasurement = now;
		}

	}

	/** Nastane, ak sa zmeni velkost okna. */
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2();

		if (height <= 0) {
			height = 1;
		}

		gl.glViewport(0, 0, width, height);

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();

		movementListener.setWidthHeight(width, height);
		proportion = (double) width / (double) height;
		glu.gluPerspective(45.0, proportion, 0.1, 160.0);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();

		Log.info("Display size changed.");
	}

	public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2) {
	}

	@Override
	public void dispose(GLAutoDrawable arg0) {
	}

}
