package sk.tekel04.scorched.game;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import com.jogamp.opengl.GL2;

/**
 * Spracuva pouzivatelsky vstup grafickeho okna. Ulohou triedy je posuvanie kamery pomocou mysi a klavesnice. Jedina
 * informacia z hry je vyska v danom mieste krajiny, takze sa kamera neocitne pod urovnou terenu. Inac je tato trieda od
 * triedy {@link Game} nezavisla.
 */
public class MovementListener implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {

	/** Maximalna povolena vzdialenost kamery od hernej plochy. */
	private static final double CAMERA_BOUNDARY = 20;

	/** Sirka okna. */
	private double width;

	/** Vyska okna. */
	private double height;

	/** Pozicia kamery. */
	public static double cameraX;

	/** Pozicia kamery. */
	public static double cameraY;

	/** Pozicia kamery. */
	public static double cameraZ;

	/** Naklonenie kamery. */
	private double xrot;
	/** Otocenie kamery. */
	private double yrot;

	/** Pole stlacenych klavesov. */
	private boolean[] keys = new boolean[256];

	/** Bod v okne, kde naposledy pouzivatel klikol. */
	private Point pointClicked;

	/** Bod v okne, kde naposledy pouzivatel zacal tahat mys. */
	private Point pointPressed;

	/** Naposledy stlacene tlacidlo mysi. */
	private int buttonPressed;

	/** Objekt na urcovanie vysky krajiny v danom bode. */
	private final Land.LandMeter landMeter;

	/**
	 * Konstruktor nastavi objekt, pomocou ktoreho vieme urcovat vysku krajiny.
	 *
	 * @param landMeter objekt urcujuci vysku krajiny
	 */
	public MovementListener(Land.LandMeter landMeter) {
		this.landMeter = landMeter;
		resetPosition();
	}

	/** Umiestni kameru na zaciatocnu poziciu v rohu mapy. */
	public void resetPosition() {
		cameraX = 0.5;
		cameraY = 5;
		cameraZ = 0.5;
		xrot = 10;
		yrot = 225;
	}

	/**
	 * Ulozi velkost okna. Udaj o velkosti okna je dolezity, aby posun mysou cez cele okno prekonal vzdy rovnaku
	 * vzdialenost na mape. V opacnom pripade by sa kamera vo vacsom okne posuvala rychlejsie.
	 *
	 * @param width  nova velkost okna v pixeloch
	 * @param height nova vyska okna v pixeloch
	 * @see GraphicsListener#reshape(javax.media.opengl.GLAutoDrawable, int, int, int, int)
	 */
	public void setWidthHeight(int width, int height) {
		this.width = width / 80.0;
		this.height = height / 80.0;
	}

	/**
	 * Posunie kameru na pouzivatelom zadanie miesto, pripadne nad toto miesto, ak by sa kamera nachadzala pod urovnou
	 * terenu. V skutocnosti posunie zaciatok suradnicoveho systemu z bodu (0, 0, 0) na miesto opacne k miestu kamery,
	 * cim vznika dojem, ze sa posunula kamera. Tuto metodu je nutne volat po kazdom zmazani obrazovky, lebo inak by
	 * obraz zostal v bode (0, 0, 0). Tato metoda spracuje aj vstup z klavesnice.
	 *
	 * @see GraphicsListener#display(javax.media.opengl.GLAutoDrawable)
	 */
	public void moveScreen(GL2 gl) {
		gl.glLoadIdentity();

		keyboardMovement();

		checkCameraBoundary();

		gl.glRotated(xrot, 1.0, 0, 0);
		gl.glRotated(360.0 - yrot, 0, 1, 0);
		gl.glTranslated(-cameraX, -0.5 - cameraY, -cameraZ);
	}

	/** Zabrani kamere odist prilis daleko od plochy alebo prepadnut sa pod teren. */
	private void checkCameraBoundary() {
		int fieldSize = landMeter.getFieldSize();

		if (cameraX < -CAMERA_BOUNDARY) {
			cameraX = -CAMERA_BOUNDARY;
		} else if (cameraX > fieldSize + CAMERA_BOUNDARY) {
			cameraX = fieldSize + CAMERA_BOUNDARY;
		}

		if (cameraZ < -CAMERA_BOUNDARY) {
			cameraZ = -CAMERA_BOUNDARY;
		} else if (cameraZ > fieldSize + CAMERA_BOUNDARY) {
			cameraZ = fieldSize + CAMERA_BOUNDARY;
		}

		double height = landMeter.height(cameraX, cameraZ);
		if (cameraY < height) {
			cameraY = height;
		} else {
			double maxHillHeight = landMeter.getMaxHillHeight();
			if (cameraY > maxHillHeight + CAMERA_BOUNDARY) {
				cameraY = maxHillHeight + CAMERA_BOUNDARY;
			}
		}
	}

	/** Posunie kameru v krajine o danu vzdialenost dozadu v smere aktualneho otocenia. */
	private void walkBack(double distance) {
		cameraX += Math.sin(yrot * Math.PI / 180) * distance;
		cameraZ += Math.cos(yrot * Math.PI / 180) * distance;
	}

	/**
	 * Posunie kameru v krajine o danu vzdialenost smerom dolava v smere aktualneho otocenia. Otocenie kamery sa
	 * nezmeni.
	 */
	private void walkLeft(double distance) {
		cameraX += Math.sin((yrot - 90) * Math.PI / 180) * distance;
		cameraZ += Math.cos((yrot - 90) * Math.PI / 180) * distance;
	}

	/** Spracuje vstup z klavesnice a prislusne posunie kameru. */
	private void keyboardMovement() {
		if (keys[KeyEvent.VK_RIGHT]) {
			if (keys[KeyEvent.VK_CONTROL]) {
				walkLeft(-0.2 * Game.TIME);
			} else {
				yrot -= 3.0 * Game.TIME;
			}
		}

		if (keys[KeyEvent.VK_LEFT]) {
			if (keys[KeyEvent.VK_CONTROL]) {
				walkLeft(0.2 * Game.TIME);
			} else {
				yrot += 3.0 * Game.TIME;
			}
		}

		if (keys[KeyEvent.VK_INSERT]) {
			xrot -= 3.0 * Game.TIME;
		}

		if (keys[KeyEvent.VK_DELETE]) {
			xrot += 3.0 * Game.TIME;
		}

		if (keys[KeyEvent.VK_PAGE_UP]) {
			cameraY += 0.2 * Game.TIME;
		}

		if (keys[KeyEvent.VK_PAGE_DOWN]) {
			cameraY -= 0.2 * Game.TIME;
		}

		if (keys[KeyEvent.VK_UP]) {
			walkBack(-0.2 * Game.TIME);
		}

		if (keys[KeyEvent.VK_DOWN]) {
			walkBack(0.20 * Game.TIME);
		}
	}

	/** Nastava, ked bol stlaceny klaves na klavesnici. */
	@Override
	public void keyPressed(KeyEvent key) {
		if (key.getKeyCode() < keys.length) {
			keys[key.getKeyCode()] = true;
		}
	}

	/** Nastava, ked bol pusteny klaves na klavesnici. */
	@Override
	public void keyReleased(KeyEvent key) {
		if (key.getKeyCode() < keys.length) {
			keys[key.getKeyCode()] = false;
		}
	}

	/** Nastava, ked pouzivatel klikol mysou v okne bez tahania mysou. */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			pointClicked = e.getPoint();
		}
	}

	/**
	 * Vrati bod posledneho kliknutia mysou na obrazovke. Zaroven tento bod zrusi, takze dalsie volanie funkcie nevrati
	 * ziaden bod, pokial nenastane nove kliknutie mysou.
	 *
	 * @return miesto posledneho kliknutia mysou
	 */
	public Point getMousePoint() {
		Point p = pointClicked;
		pointClicked = null;
		return p;
	}

	/** Nastava, ked pouzivatel stlacil tlacidlo mysi a nasledne tahal mysou. */
	@Override
	public void mousePressed(MouseEvent e) {
		pointPressed = e.getPoint();
		buttonPressed = e.getButton();
	}

	/** Nastava, ked pouzivatel pusti tlacidlo mysi po tom, ako tahal mysou. */
	@Override
	public void mouseReleased(MouseEvent e) {
		pointPressed = null;
	}

	/**
	 * Nastava, ked pouzivatel taha mysou. Ak je stlacene lave tlacidlo mysi, kamera sa posuva v krajine, ak je stlacene
	 * prave tlacidlo mysi, kamera sa otaca bez pohybu z miesta.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		Point here = e.getPoint();
		if (here != null && pointPressed != null) {
			if (buttonPressed == MouseEvent.BUTTON1) {
				walkLeft((pointPressed.x - here.x) / width);
				walkBack((here.y - pointPressed.y) / height);
			} else {
				yrot += (pointPressed.x - here.x) / width * 2;
				xrot += (here.y - pointPressed.y) / height;
			}
		}
		pointPressed = here;
	}

	/** Nastava, ked pouzivatel toci koliesko na mysi. */
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		cameraY += 0.2 * e.getWheelRotation();
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}
