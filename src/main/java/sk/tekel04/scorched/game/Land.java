package sk.tekel04.scorched.game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.logging.Log;
import sk.tekel04.scorched.picture.SelectablePicture;
import sk.tekel04.scorched.picture.SimpleTextureLoader;

/**
 * Pevnina - kopce. Tato trieda neobsahuje dalsie objekty nachadzajuce sa v krajine, ako napriklad strely alebo tanky.
 * Metody tejto triedy umoznuju vytvaranie novej krajiny, pracu so subormi a kreslenie samotnej krajiny. Na zistenie
 * vysky v danom mieste mapy pouzite metody triedy {@link LandMeter}.
 */
public class Land implements SelectablePicture {

	/** Each square of the land is further divided into precise grid with this many squares on one side. */
	private static final int GRID_RESOLUTION = 2;

	/** Textura travy. */
	private static int texture_grass;

	/** Size of land in squares. */
	private int fieldSize;

	/** Size of precise grid. */
	private int gridSize;

	/** Vyskova mapa krajiny. */
	private double[][] height;

	/** Normalovy vektor, kolmy na krajinu v danom bode. */
	private double[][][] normal;

	/** Vyska najvyssieho kopca na zaciatku hry. */
	private double maxHillHeight;

	/**
	 * Vnorena trieda LandMeter poskytuje metody tykajuce sa krajiny zaujimave pre priebeh hry, ako napriklad zistenie
	 * vysky v lubovolnom mieste krajiny. Okrem toho metody tejto triedy predstavuju zabezpecenie, aby pocas hry nikto
	 * nemohol menit krajinu, alebo napriklad nacitat novu krajinu pocas hry.
	 */
	public class LandMeter {

		public int getFieldSize() {
			return fieldSize;
		}

		public double getMaxHillHeight() {
			return maxHillHeight;
		}

		/**
		 * Vrati vysku terenu v danom bode krajiny. Vrati 0, ak je v danom mieste len voda. Dany bod sa moze nachadzat
		 * za okrajom krajiny, okolo krajiny sa tiahne nekonecna hladina vody.
		 *
		 * @param x x-suradnica daneho bodu v rovine
		 * @param y y-suradnica daneho bodu v rovine
		 * @return vyska terenu v danom mieste krajiny
		 */
		public double height(double x, double y) {
			if (x <= 0 || y <= 0 || x >= fieldSize - 1 || y >= fieldSize - 1) {
				return 0;
			}

			final double gridX = x * GRID_RESOLUTION;
			final double gridY = y * GRID_RESOLUTION;
			final int i = (int) gridX;
			final int j = (int) gridY;

			// Kazdy stvorec krajiny sa sklada z dvoch trojuholnikov. Musime zistit, v ktorom trojuholniku sa nachadza
			// dany bod. Vypocitame smerove vektory roviny tohto trojuholnika a vypocitame prienik roviny so zvislou
			// priamkou prechadzajucou danym bodom na mape.

			if (gridX - i + gridY - j > 1) {
				final double di = height[i][j + 1] - height[i + 1][j + 1];
				final double dj = height[i + 1][j] - height[i + 1][j + 1];
				return Math.max(0, height[i + 1][j + 1] + di * (i + 1 - gridX) + dj * (j + 1 - gridY));
			} else {
				final double di = height[i + 1][j] - height[i][j];
				final double dj = height[i][j + 1] - height[i][j];
				return Math.max(0, height[i][j] + di * (gridX - i) + dj * (gridY - j));
			}
		}

		/**
		 * Zisti, ci sa na danom policku krajiny nachadza aspon kusok vody. Stvorec leziaci mimo mapu obsahuje vzdy
		 * vodu. Stvorec obsahuje vodu, ak aspon jeden z jeho vrcholov je na urovni vodnej hladiny, alebo pod nou.
		 *
		 * @param fieldI lava suradnica stvorca
		 * @param fieldJ horna suradnica stvorca
		 * @return true ak dany stvorec obsahuje aspon kusok vody, inak false
		 */
		public boolean isWater(final int fieldI, final int fieldJ) {
			if (fieldI < 0 || fieldJ < 0 || fieldI >= fieldSize || fieldJ >= fieldSize) {
				return true;
			}

			final int gridI = fieldI * GRID_RESOLUTION;
			final int gridJ = fieldJ * GRID_RESOLUTION;

			for (int i = gridI; i <= gridI + GRID_RESOLUTION; i++) {
				for (int j = gridJ; j <= gridJ + GRID_RESOLUTION; j++) {
					if (height[i][j] <= 0) {
						return true;
					}
				}
			}

			return false;
		}

		/**
		 * Zarovna povrch krajiny na danom jednotkovom stvorci a vrati vysku terenu po zarovnani. Nova vyska je rovna
		 * priemeru vysok vsetkych vrcholov v stvorci. Ked je stvorec zarovnany, tak funkcia
		 * {@link #height(double, double)} vrati pre lubovolny bod vnutri stvorca rovnaku hodnotu. Ak je odchylka vysok
		 * jednotlivych vrcholov stvorca zanedbatelne mala, tato funkcia nevykona ziadne zarovanie. Drobne odchylky pri
		 * pocitani s typom {@link Double} by sa pri castom zarovnavani kumulovali. Vracia novu vysku po zarovnani. Ak
		 * je po zarovnani stvorec nad vodou, tato hodnota je rovna hodnote funkcie {@link #height(double, double)}.
		 * Narozdiel od nej vsak tato metoda vracia vysku krajiny aj pod vodou, a nie 0.
		 *
		 * @param fieldI lava suradnica stvorca
		 * @param fieldJ horna suradnica stvorca
		 * @return priemerna vyska vrcholov daneho stvorca
		 */
		public double makePlatform(final int fieldI, final int fieldJ) {
			if (fieldI < 0 || fieldJ < 0 || fieldI >= fieldSize || fieldJ >= fieldSize) {
				return 0;
			}

			final int gridI = fieldI * GRID_RESOLUTION;
			final int gridJ = fieldJ * GRID_RESOLUTION;

			double sum = 0;
			for (int i = gridI; i <= gridI + GRID_RESOLUTION; i++) {
				for (int j = gridJ; j <= gridJ + GRID_RESOLUTION; j++) {
					sum += height[i][j];
				}
			}

			final double average = sum / (GRID_RESOLUTION + 1) / (GRID_RESOLUTION + 1);

			double offset = 0;
			for (int i = gridI; i <= gridI + GRID_RESOLUTION; i++) {
				for (int j = gridJ; j <= gridJ + GRID_RESOLUTION; j++) {
					offset += Math.abs(height[i][j] - average);
				}
			}

			if (offset / (GRID_RESOLUTION + 1) / (GRID_RESOLUTION + 1) > 0.001) {
				for (int i = gridI; i <= gridI + GRID_RESOLUTION; i++) {
					for (int j = gridJ; j <= gridJ + GRID_RESOLUTION; j++) {
						height[i][j] = average;
					}
				}
			}

			return average;
		}

		/**
		 * Vytvori gulovy krater okolo daneho epicentra. Hoci vybuch je gulovy, vysledny krater nebude gula, ale
		 * splostena gula, znicena bude 1 / 3 zasiahnutej hmoty.
		 *
		 * @param x x-suradnica epicentra vybuchu
		 * @param y y-suradnica epicentra vybuchu
		 * @param z z-suradnica epicentra vybuchu
		 * @param r polomer vybuchu
		 */
		public void makeCrater(final double x, final double y, final double z, final double r) {
			double gridI = GRID_RESOLUTION * x;
			double gridJ = GRID_RESOLUTION * z;
			double gridR = GRID_RESOLUTION * r;
			for (int i = (int) (gridI - gridR - 1); i <= (int) (gridI + gridR + 1); i++) {
				for (int j = (int) (gridJ - gridR - 1); j <= (int) (gridJ + gridR + 1); j++) {
					if (i >= 0 && j >= 0 && i < gridSize && j < gridSize) {

						double i1 = (double) i / GRID_RESOLUTION;
						double j1 = (double) j / GRID_RESOLUTION;
						double dy = Math.sqrt(r * r - (x - i1) * (x - i1) - (z - j1) * (z - j1));

						if (Double.isNaN(dy)) {
							continue;
						}

						double y1 = y - dy;
						double y2 = y + dy;
						double destroyed;

						if (height[i][j] > y1) {
							if (height[i][j] > y2) {
								destroyed = 2 * dy;
							} else {
								destroyed = height[i][j] - y1;
							}
							height[i][j] -= destroyed / 3;
						}
					}
				}
			}
		}
	}

	/** Nastavi vsetkym premennym popisujucim krajinu danu velkost. */
	private void initSize(final int fieldSize) {
		this.fieldSize = fieldSize;

		// Vysky si pamatame len na priesecnikoch poludnikov a rovnobeziek.
		// 5 hernych poludnikov vytvori 4 herne policka.
		// Ak GRID_RESOLUTION = 3, tak potom mame dohromady 12 grid policok.
		// Tie vyzaduju 13 grid poludnikov.
		gridSize = Math.max(GRID_RESOLUTION * (fieldSize - 1) + 1, 0);

		height = new double[gridSize][gridSize];
		normal = new double[gridSize][gridSize][3];
	}

	/**
	 * Vyprazdni krajinu. Na takejto krajine nie je mozne odohrat hru. Tuto funkciu nie je nutne volat pred nacitanim
	 * novej krajiny.
	 */
	public void empty() {
		initSize(0);
	}

	/**
	 * Vytvori novu nahodnu krajinu s danymi parametrami. Ak su parametre chybne zadane, stara krajina zostava
	 * nezmenena. Parametre musia splnat nasledujuce podmienky:
	 * <ul>
	 * <li>size je viac ako 0, inak by bola krajina prazdna</li>
	 * <li>water je cislo od 0 do 100, pretoze ide o percento</li>
	 * <li>hills je viac ako 0, inak by krajina bola rovina na urovni hladiny</li>
	 * </ul>
	 *
	 * @param fieldSize rozmer novej krajiny v hernych polickach
	 * @param hills     vyska kopcov, 100 je standard
	 * @param water     povrch krajiny pokryty vodou v percentach
	 * @throws WrongGameSettingsException ak su vstupne parametre nespravne
	 */
	public void createRandom(final int fieldSize, final int hills, final int water) throws WrongGameSettingsException {

		if (fieldSize <= 0 || water < 0 || water > 100 || hills <= 0) {
			throw new WrongGameSettingsException("The parameters for generating a random land are wrong.");
		}

		/** Obsahuje metody na vytvorenie novej nahodnej mapy podla zadanych parametrov. */
		class RandomCreator {

			/**
			 * Tato metoda vypocita hodnotu funkcie kosinus zrotovanej podla zvislej osi. <code>f(0, 0)</code>
			 * predstavuje maximum, body v rovnakej vzdialenosti od (0, 0) maju rovnaku funkcnu hodnotu. Pre body dalej
			 * ako 1 je funkcna hodnota rovna 0. Nakoniec je vysledok vynasobeny cislom <code>hills / 100</code>.
			 *
			 * @param x x-suradnica v rovine
			 * @param y y-suradnica v rovine
			 * @return hodnota funkcie cos zrotovanej podla zvislej osi
			 */
			private double cosv(final double x, final double y) {
				final double distance = Math.sqrt(x * x + y * y);
				if (distance > 1) {
					return 0;
				}
				return 1 + Math.cos(distance * Math.PI);
			}

			/** Vytvori v krajine kopec s danym stredom, vyskou a polomerom. */
			private void makeHill(double fieldX, double fieldY, double fieldRadius, double hillHeight) {
				final double gridRadius = fieldRadius * GRID_RESOLUTION;
				final double gridX = fieldX * GRID_RESOLUTION;
				final double gridY = fieldY * GRID_RESOLUTION;

				for (int i = (int) (gridX - gridRadius - 1); i <= (int) (gridX + gridRadius + 1); i++) {
					for (int j = (int) (gridY - gridRadius - 1); j <= (int) (gridY + gridRadius + 1); j++) {
						if (i >= 0 && j >= 0 && i < gridSize && j < gridSize) {
							height[i][j] += hillHeight * cosv((i - gridX) / gridRadius, (j - gridY) / gridRadius);
						}
					}
				}
			}

			/** Vrati nahodne realne cislo od 0 do daneho cisla. */
			private double random(double max) {
				return max * Math.random();
			}

			/** Posunie krajinu smerom dolu tak, aby voda zakryla prave tolko plochy krajiny, ako bolo zadane. */
			private void sinkDown() {
				// Zoradi vsetky body v krajine podla vysky, a vyberie bod podla daneho percenta vody v krajine. Vodna
				// hladina bude prechadzat cez tento bod.

				final double[] heights = new double[gridSize * gridSize];
				for (int j = 0; j < gridSize; j++) {
					for (int i = 0; i < gridSize; i++) {
						heights[gridSize * j + i] = height[i][j];
					}
				}

				Arrays.sort(heights);
				final double seaLevel = heights[gridSize * gridSize * water / 100];

				for (int j = 0; j < gridSize; j++) {
					for (int i = 0; i < gridSize; i++) {
						height[i][j] -= seaLevel;
					}
				}

				maxHillHeight = heights[heights.length - 1] - seaLevel;
			}

			/** Vytvori nahodnu krajinu danych rozmerov. */
			private void createLand() {
				for (int i = 0; i < gridSize; i++) {
					Arrays.fill(height[i], 0);
				}

				// Pocet kopcov v krajine je priamo umerny jej ploche
				for (int i = 0; i < fieldSize * fieldSize / 12; i++) {
					final double fieldR = 3 + random(fieldSize / 10.0);
					final double fieldX = fieldR + random(fieldSize - 2 * fieldR);
					final double fieldY = fieldR + random(fieldSize - 2 * fieldR);

					makeHill(fieldX, fieldY, fieldR, (0.1 + random(0.5)) * hills / 100.0);
				}

				sinkDown();
				makeBorderOfWater();
			}

		}

		initSize(fieldSize);

		new RandomCreator().createLand();

		try {
			saveToFile(Paths.get("random_land.txt"));
		} catch (IOException e) {
			Log.exception("Exception thrown while saving land", e);
		}
	}

	/**
	 * Ulozi krajinu do suboru.
	 *
	 * @param fileWithLand subor, do ktoreho ulozit krajinu
	 */
	public void saveToFile(Path fileWithLand) throws IOException {
		try (PrintWriter out = new PrintWriter(Files.newBufferedWriter(fileWithLand))) {
			out.println(GRID_RESOLUTION);
			out.println(fieldSize);

			for (int j = 0; j < gridSize; j++) {
				for (int i = 0; i < gridSize; i++) {
					out.format(Locale.ENGLISH, "%.2f ", height[i][j]);
				}
				out.println();
			}
		}

		Log.info("Land saved into file " + fileWithLand);
	}

	/**
	 * Nacita krajinu z daneho suboru. V pripade, ze sa krajinu nepodarilo nacitat, stara krajine je uz stratena.
	 *
	 * @param fileWithLand subor, z ktoreho nacitat krajinu.
	 */
	public void loadFromFile(Path fileWithLand) throws IOException {
		try (BufferedReader in = Files.newBufferedReader(fileWithLand)) {

			final int gridResolution = Integer.parseInt(in.readLine());
			if (GRID_RESOLUTION != gridResolution) {
				throw new IllegalArgumentException("This land has incompatible grid resolution");
			}

			final int fieldSize = Integer.parseInt(in.readLine());
			initSize(fieldSize);

			for (int j = 0; j < gridSize; j++) {
				String[] numbers = in.readLine().split("[ ]+");
				for (int i = 0; i < gridSize; i++) {
					height[i][j] = Double.parseDouble(numbers[i]);
				}
			}

			maxHillHeight = Arrays.stream(height).flatMapToDouble(x -> Arrays.stream(x)).max().getAsDouble();
		}

		Log.info("Land loaded from file " + fileWithLand);

		makeBorderOfWater();
	}

	/** Vytvori okolo krajiny ram z vody, aby nebolo mozne vidiet pod uroven terenu. */
	private void makeBorderOfWater() {
		// Kedze trojuholniky na kraji gridu sa nevykresluju, musia byt cele pod vodou. Teda dva poludniky.
		for (int i = 0; i < gridSize; i++) {
			height[0][i] = Math.min(-2 * Water.MAX_WAVE, height[0][i]);
			height[1][i] = Math.min(-Water.MAX_WAVE, height[1][i]);
			height[i][0] = Math.min(-2 * Water.MAX_WAVE, height[i][0]);
			height[i][1] = Math.min(-Water.MAX_WAVE, height[i][1]);
			height[gridSize - 1][i] = Math.min(-2 * Water.MAX_WAVE, height[gridSize - 1][i]);
			height[gridSize - 2][i] = Math.min(-Water.MAX_WAVE, height[gridSize - 2][i]);
			height[i][gridSize - 1] = Math.min(-2 * Water.MAX_WAVE, height[i][gridSize - 1]);
			height[i][gridSize - 2] = Math.min(-Water.MAX_WAVE, height[i][gridSize - 2]);
		}
	}

	/**
	 * Rozmiestni v krajine dany pocet tankov na nahodne miesta. Krajina nie je ziadnym sposobom pozmenena. Tato metoda
	 * vytvori dany pocet novych tankov, a kazdemu nastavi jedinecny identifikator, aby ich bolo mozne rozoznat. Tento
	 * identifikator je zhodny s indexom v danom vektore.
	 *
	 * @param tanks           {@link List}, do ktoreho bude umiestnovat tanky
	 * @param numberOfPlayers pocet hracov
	 * @param numberOfTanks   pocet tankov pre kazdeho hraca
	 * @throws WrongGameSettingsException Sprava pre pouzivatela ak sa do krajiny nezmesti tento pocet tankov
	 */
	public void placeTanks(List<Tank> tanks, int numberOfPlayers, int numberOfTanks) throws WrongGameSettingsException {

		if (fieldSize == 0) {
			throw new WrongGameSettingsException("Land is empty. Generate a random land or load one from a file.");
		}

		/** Reprezentacia jedneho policka na mape. */
		class Place {
			/** x-suradnica v krajine */
			public final int i;

			/** y-suradnica v krajine */
			public final int j;

			public Place(int i, int j) {
				this.i = i;
				this.j = j;
			}
		}

		LandMeter landMeter = new LandMeter();
		List<Place> places = new ArrayList<>();

		// Ziadne dva tanky nesmu byt presne vedla seba, inak by si navzajom zdeformovali plosinu, na ktorej stoja. Tato
		// vlastnost je dosiahnuta tak, ze tanky sa rozmiestnuju s krokom velkosti 2. Je teda mozne, ze hoci krajina
		// obsahuje dost volnych policok, tanky nie je mozne rozmiestnit, lebo pocet volnych policok v kazdom druhom
		// rade a stlpci nie je dostatocny. Tank sa navyse nemoze nachadzat na hracom policku na kraji plochy - ten je
		// povinne pod vodou z grafickych dovodov

		// Deterministicky voli nahodne parne alebo neparne riadky a stlpce.
		for (int j = 1 + ((int) (height[0][0] * 100)) % 2; j < fieldSize - 2; j += 2) {
			for (int i = 1 + ((int) (height[j][j] * 100)) % 2; i < fieldSize - 2; i += 2) {
				if (!landMeter.isWater(i, j)) {
					places.add(new Place(i, j));
				}
			}
		}

		if (places.size() < numberOfPlayers * numberOfTanks) {
			throw new WrongGameSettingsException("This land is too small for that many tanks and players");
		}

		Collections.shuffle(places);

		for (int id = 0; id < numberOfPlayers * numberOfTanks; id++) {
			Place place = places.get(id);
			Tank tank = new Tank(id, id / numberOfTanks, place.i, place.j);
			tanks.add(tank);
		}
	}

	/** Nacita texturu travy na kopce. */
	public static void loadTexture(GL2 gl) {
		SimpleTextureLoader loader = new SimpleTextureLoader();
		texture_grass = loader.loadTexture(gl, "grass.png");
	}

	/** Nakresli kopce a hladinu krajiny. */
	@Override
	public void draw(GL2 gl, boolean select) {
		if (select) {
			gl.glLoadName(-1);
		}

		gl.glBindTexture(GL2.GL_TEXTURE_2D, texture_grass);

		drawLandWithShadedTriangles(gl);
	}

	/**
	 * Nakresli kopce. Najprv spocita pre kazdy bod v krajine normalovy vektor kolmy na krajinu v tomto bode. Potom
	 * kresli trojuholniky, a pouzije tieto normalove vektory na ich vytienovanie.
	 */
	private void drawLandWithShadedTriangles(final GL2 gl) {
		for (int j = 1; j < gridSize - 1; j++) {
			for (int i = 1; i < gridSize - 1; i++) {
				// spocita normalovy vektor v bode [i, j]
				final double nx = 2 * (height[i + 1][j] - height[i - 1][j]);
				final double ny = -4.0 / GRID_RESOLUTION;
				final double nz = 2 * (height[i][j + 1] - height[i][j - 1]);

				final double d = Math.sqrt(nx * nx + ny * ny + nz * nz);

				normal[i][j][0] = nx / d;
				normal[i][j][1] = ny / d / 2; // umyselna deformacia pre zvyraznenie tienov
				normal[i][j][2] = nz / d;
			}
		}

		gl.glBegin(GL2.GL_TRIANGLES);
		for (int j = 1; j < gridSize - 2; j++) {
			for (int i = 1; i < gridSize - 2; i++) {
				triangleShaded(gl, i, j, i + 1, j, i, j + 1, 0);
				triangleShaded(gl, i + 1, j, i + 1, j + 1, i, j + 1, 1);
			}
		}
		gl.glEnd();
	}

	/**
	 * Nakresli jeden trojuholnik krajiny. Trojuholnik, ktoreho vsetky vrcholy lezia pod urovnou hladiny, nebude
	 * nakresleny.
	 *
	 * @param gl   grafika, kam sa ma kreslit
	 * @param ai   x-suradnica 1. vrcholu
	 * @param aj   y-suradnica 1. vrcholu
	 * @param bi   x-suradnica 2. vrcholu
	 * @param bj   y-suradnica 2. vrcholu
	 * @param ci   x-suradnica 3. vrcholu
	 * @param cj   y-suradnica 3. vrcholu
	 * @param part polovica textury, ktora sa nakresli (0 alebo 1)
	 */
	private void triangleShaded(GL2 gl, int ai, int aj, int bi, int bj, int ci, int cj, int part) {
		if (height[ai][aj] < -Water.MAX_WAVE && height[bi][bj] < -Water.MAX_WAVE && height[ci][cj] < -Water.MAX_WAVE) {
			return;
		}

		gl.glNormal3dv(normal[ai][aj], 0);
		gl.glTexCoord2i(0, part);
		gl.glVertex3d((double) ai / GRID_RESOLUTION, height[ai][aj], (double) aj / GRID_RESOLUTION);

		gl.glNormal3dv(normal[bi][bj], 0);
		gl.glTexCoord2i(part, 1);
		gl.glVertex3d((double) bi / GRID_RESOLUTION, height[bi][bj], (double) bj / GRID_RESOLUTION);

		gl.glNormal3dv(normal[ci][cj], 0);
		gl.glTexCoord2i(1, 0);
		gl.glVertex3d((double) ci / GRID_RESOLUTION, height[ci][cj], (double) cj / GRID_RESOLUTION);
	}

}
