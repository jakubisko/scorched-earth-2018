package sk.tekel04.scorched.game;

/**
 * Throw when user tries to generate a land with illegal settings, such as negative water; or when user tries to start
 * the game with illegal settings, such as more tanks that fit the map. This is always a problem on user's site.
 *
 * Use method getMessage() to retrieve the text that should be displayed to the user.
 */
public class WrongGameSettingsException extends Exception {

	private static final long serialVersionUID = -6248003566637141038L;

	/** @param message text that will be displayed to the user */
	public WrongGameSettingsException(String message) {
		super(message);
	}

}
