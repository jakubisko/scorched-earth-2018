package sk.tekel04.scorched.game;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.SimpleTextureLoader;
import sk.tekel04.scorched.picture.UnselectablePicture;

/** Kresli vodnu hladinu. Nema interakciu zo strelami. */
public class Water implements UnselectablePicture {

	/** Maximalna fyzicka vyska vlny, do ktorej realne vystupi polynom. */
	public static final double MAX_WAVE = 0.04;

	/** Dlzka vlny v hernych polickach. */
	private static final double WAVE_LENGTH = 3;

	/** Dlzka textury v hernych polickach. MUSI BYT NASOBKOM DLZKY VLNY! Inak bude textura pri pohybe kamerou skakat. */
	private static final double TEXTURE_LENGTH = 3;

	/** Rozlisenie - jedna vlna bude rozdelena na tento pocet polynomov (frekvencia samplovania sin funkcie). */
	private static final int QUADS_PER_WAVE = 15;

	/** Animovana nie je cela hladina do nekonecna, ale len do tejto vzdialenosti od kamery. */
	private static final double ANIMATION_DISTANCE = 50;

	/** Textura vody. */
	private static int texture_water;

	/** Faza vlny - od 0 do 2 PI. */
	private double phase;

	/** Textura sa pomaly hybe v smere vetra. */
	private double textureOffset;

	/** Nakresli hladinu. */
	@Override
	public void draw(GL2 gl) {
		phase += 0.1 * Game.TIME;
		if (phase > 2 * Math.PI) {
			phase -= 2 * Math.PI;
		}

		textureOffset += 0.001 * Game.TIME;
		if (textureOffset > 1) {
			textureOffset--;
		}

		gl.glPushMatrix();

		// Voda sa hybe s kamerou; ale len v nasobkoch textury, aby vzdy poskocila na periodicky rovnake miesto.
		double cameraX = Math.round(MovementListener.cameraX / TEXTURE_LENGTH) * TEXTURE_LENGTH;
		double cameraZ = Math.round(MovementListener.cameraZ / TEXTURE_LENGTH) * TEXTURE_LENGTH;
		gl.glTranslated(cameraX, 0, cameraZ);

		gl.glBindTexture(GL2.GL_TEXTURE_2D, texture_water);
		gl.glBegin(GL2.GL_QUADS);
		gl.glNormal3d(0, -1, 0);

		drawHorizont(gl);
		drawWaves(gl);

		gl.glEnd();

		gl.glPopMatrix();
	}

	/** Nakresli vlny. Plocha vln je animovana, konecne velka. */
	private void drawWaves(GL2 gl) {

		// Pre N polynomov potrebujeme N + 1 vysok.
		double height[] = new double[QUADS_PER_WAVE + 1];
		for (int i = 0; i < height.length; i++) {
			height[i] = MAX_WAVE * Math.sin(phase + 2 * Math.PI * i / QUADS_PER_WAVE);
		}

		for (int i = 0; i < QUADS_PER_WAVE; i++) {
			for (double offsetX = -ANIMATION_DISTANCE; offsetX < ANIMATION_DISTANCE; offsetX += WAVE_LENGTH) {
				quadShaded(gl, offsetX + i * WAVE_LENGTH / QUADS_PER_WAVE, height[i], height[i + 1]);
			}
		}
	}

	/**
	 * Nakresli jeden obdlznik vody.
	 *
	 * @param offsetX x suradnica zaciatku obdlznika
	 * @param h1      vyska na jednom konci oblznika
	 * @param h2      vyska na druhom konci oblznika
	 */
	private void quadShaded(GL2 gl, double offsetX, double h1, double h2) {
		textureCoordsForXY(gl, 2 * ANIMATION_DISTANCE, offsetX);
		gl.glVertex3d(offsetX, h1, ANIMATION_DISTANCE);

		textureCoordsForXY(gl, 2 * ANIMATION_DISTANCE, offsetX + WAVE_LENGTH / QUADS_PER_WAVE);
		gl.glVertex3d(offsetX + WAVE_LENGTH / QUADS_PER_WAVE, h2, ANIMATION_DISTANCE);

		textureCoordsForXY(gl, 0, offsetX + WAVE_LENGTH / QUADS_PER_WAVE);
		gl.glVertex3d(offsetX + WAVE_LENGTH / QUADS_PER_WAVE, h2, -ANIMATION_DISTANCE);

		textureCoordsForXY(gl, 0, offsetX);
		gl.glVertex3d(offsetX, h1, -ANIMATION_DISTANCE);
	}

	/** Nakresli horizont - velku plochu vody daleko od kamery, ktora nie je animovana. */
	private void drawHorizont(GL2 gl) {
		textureCoordsForXY(gl, 300, 300);
		gl.glVertex3d(300, -2 * MAX_WAVE, 300);

		textureCoordsForXY(gl, -300, 300);
		gl.glVertex3d(300, -2 * MAX_WAVE, -300);

		textureCoordsForXY(gl, -300, -300);
		gl.glVertex3d(-300, -2 * MAX_WAVE, -300);

		textureCoordsForXY(gl, 300, -300);
		gl.glVertex3d(-300, -2 * MAX_WAVE, 300);
	}

	private void textureCoordsForXY(GL2 gl, double x, double y) {
		gl.glTexCoord2d(x / TEXTURE_LENGTH, textureOffset + y / TEXTURE_LENGTH);
	}

	/** Nacita texturu vodnej hladiny. */
	public static void loadTexture(GL2 gl) {
		SimpleTextureLoader loader = new SimpleTextureLoader();
		texture_water = loader.loadTexture(gl, "water.jpg");
	}

}
