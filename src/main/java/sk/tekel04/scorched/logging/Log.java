package sk.tekel04.scorched.logging;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/** Class for logging. */
public class Log {

	private static Logger logger = Logger.getLogger("sk.tekel04.scorched");

	/** Logs debug info. */
	public static void info(final String text) {
		logger.info(text);
	}

	/** Logs warning. */
	public static void warning(final String text) {
		logger.warning(text);
	}

	/** Logs throwable. */
	public static void exception(final String text, final Throwable throwable) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		throwable.printStackTrace(pw);
		String sStackTrace = sw.toString();

		logger.severe(text + '\n' + sStackTrace);
	}

	/** Logs method entering. */
	public static void entering(final String sourceClass, final String sourceMethod) {
		logger.entering(sourceClass, sourceMethod);
	}

	/** Logs method leaving. */
	public static void exiting(final String sourceClass, final String sourceMethod) {
		logger.exiting(sourceClass, sourceMethod);
	}

	/** Creates logger. */
	static {
		try {
			DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss SSS");
			FileHandler fh = new FileHandler("log.txt");
			fh.setFormatter(new Formatter() {

				@Override
				public String format(final LogRecord record) {
					final StringBuilder sb = new StringBuilder();

					LocalDateTime time = LocalDateTime.ofInstant(Instant.ofEpochMilli(record.getMillis()),
							ZoneId.systemDefault());
					sb.append(timeFormatter.format(time)).append('\t');
					sb.append(record.getLevel()).append('\t');

					final String message = record.getMessage();
					sb.append(message);

					if ("ENTRY".equals(message) || "RETURN".equals(message)) {
						sb.append(' ').append(record.getSourceClassName());
						sb.append('.').append(record.getSourceMethodName()).append("()");
					}

					sb.append('\n');

					return sb.toString();
				}
			});
			logger.addHandler(fh);
			logger.setLevel(Level.ALL);
		} catch (final SecurityException e) {
			System.err.println("Unable to create logger: SecurityException");
		} catch (final IOException e) {
			System.err.println("Unable to create logger: IOException");
		}
	}

}
