package sk.tekel04.scorched.arrow;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * {@link JPanel} wrapper okolo {@link CanvasArrow}. Je to hotovy graficky komponent na zobrazenie cisel v rozsahu 90
 * alebo 360 jednotiek, umoznujuci pouzivatelovi menit hodnotu pomocou mysi alebo tlacidiel.
 */
public class PanelArrow extends JPanel {
	private static final long serialVersionUID = -5489900755096091050L;

	/** Komponent, od ktoreho prijimame vstup. */
	private CanvasArrow canvasArrow;

	/** Komponent, ktory zobrazuje aktualnu hodnotu. */
	private JLabel labelValue;

	/** Tlacidlo na zvysenie hodnoty. */
	private JButton buttonPlus;

	/** Tlacidlo na znizenie hodnoty. */
	private JButton buttonMinus;

	/** Text, ktory sa bude zobrazovat za hodnotou, napr. "km". */
	private String valueSuffix = "";

	/**
	 * Vytvori novy komponent PanelArrow s danym nadpisom a rozsahom.
	 *
	 * @param caption Nadpis panelu
	 * @param min     Minimum, ktore moze nadobudnut hodnota
	 * @param max     Maximum, ktore moze nadobudnut hodnota
	 * @throws IllegalArgumentException ak (max - min) nie je 360 alebo 90
	 */
	public PanelArrow(String caption, int min, int max) {
		if (max - min != 360 && max - min != 90) {
			throw new IllegalArgumentException();
		}

		setBorder(BorderFactory.createEtchedBorder());
		setLayout(new GridBagLayout());

		add(new JLabel(caption, JLabel.CENTER), position(0, 0, 4, 1));

		labelValue = new JLabel("", JLabel.CENTER);
		add(labelValue, position(2, 1, 2, 1));

		canvasArrow = new CanvasArrow(min, max);
		canvasArrow.setValueDisplay(this::updateLabel);
		add(canvasArrow, position(0, 1, 2, 2));

		buttonMinus = new JButton("-");
		buttonMinus.addActionListener(e -> canvasArrow.setValue(canvasArrow.getValue() - 1));
		add(buttonMinus, position(2, 2, 1, 1));

		buttonPlus = new JButton("+");
		buttonPlus.addActionListener(e -> canvasArrow.setValue(canvasArrow.getValue() + 1));
		add(buttonPlus, position(3, 2, 1, 1));
	}

	private void updateLabel(Double value) {
		labelValue.setText(value != null ? value.intValue() + valueSuffix : " ");
	}

	/**
	 * Pomocna funkcia na ulozenie na GridBagLayout.
	 *
	 * @param x      suradnica laveho horneho rohu komponentu
	 * @param y      suradnica laveho horneho rohu komponentu
	 * @param width  sirka komponentu
	 * @param height vyska komponentu
	 * @return pozicia, na ktoru ulozit komponent
	 */
	private GridBagConstraints position(int x, int y, int width, int height) {
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0 / 3.0;

		c.gridx = x;
		c.gridy = y;
		c.gridwidth = width;
		c.gridheight = height;
		return c;
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (buttonPlus != null) {
			buttonPlus.setEnabled(enabled);
		}

		if (buttonMinus != null) {
			buttonMinus.setEnabled(enabled);
		}

		canvasArrow.setEnabled(enabled);
		updateLabel(enabled ? canvasArrow.getValue() : null);
	}

	/** Nastavi text, ktory sa bude zobrazovat za hodnotou, napr. "km". */
	public void setValueSuffix(String valueSuffix) {
		this.valueSuffix = valueSuffix;
	}

	/** Nastavi aktualnu hodnotu, ktora ma byt zobrazena. */
	public void setValue(double value) {
		canvasArrow.setValue(value);
	}

	/** Aktualna hodnota zobrazovana PanelArrow. */
	public double getValue() {
		return canvasArrow.getValue();
	}

}
