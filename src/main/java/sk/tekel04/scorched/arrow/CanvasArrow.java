package sk.tekel04.scorched.arrow;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Consumer;

/**
 * Graficky komponent, ktory dokaze zobrazit cisla v rozsahu 360 jednotiek na kruhu, alebo cisla v rozsahu 90 jednotiek
 * na stvrtkruhu. Pouzivatel moze hodnotu menit pomocou kurzoru mysi. Je mozne pripojit Consumer, ktory bude zavolany
 * pri kazdej zmene hodnoty.
 *
 * @see PanelArrow
 */
public class CanvasArrow extends Canvas {
	private static final long serialVersionUID = 8081437537601472163L;

	/** Farba ukazovatela. Sedo - modra. */
	private static final Color ARROW_COLOR = new Color(112, 146, 190);

	/** Minimalna hodnota, ktoru je mozne zobrazit. */
	private final int min;

	/** Maximalna hodnota, ktoru je mozne zobrazit. */
	private final int max;

	/** Komponent, ktory zobrazuje hodnotu v textovej podobe. */
	private Consumer<Double> valueDisplay;

	/** Aktualna hodnota, pocitana od 0 v momente minima. */
	private double direction;

	/**
	 * Vytvori novy komponent CanvasArrow s danym rozsahom.
	 *
	 * @param min Minimum, ktore moze nadobudnut hodnota
	 * @param max Maximum, ktore moze nadobudnut hodnota
	 * @throws IllegalArgumentException ak (max - min) nie je 360 alebo 90
	 */
	public CanvasArrow(int min, int max) {
		if (max - min != 360 && max - min != 90) {
			throw new IllegalArgumentException();
		}

		this.max = max;
		this.min = min;

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				double x, y;
				Dimension size = getSize();

				if (max - min == 360) {
					x = size.height / 2 - e.getY();
					y = e.getX() - size.width / 2;
				} else {
					double scale = Math.min(size.width, size.height) - 2;
					x = e.getX() - (size.width - scale) / 2;
					y = (size.height + scale) / 2 - e.getY();
				}
				setValue(min + Math.atan2(y, x) * 180 / Math.PI);
			}
		});
	}

	/**
	 * Nastavi prvok, ktory bude zobrazovat aktualnu ciselnu hodnotu tohto komponentu.
	 *
	 * @param valueDisplay Consumer pre novu cislenu hodnotu
	 * @uml.property name="valueDisplay"
	 */
	public void setValueDisplay(Consumer<Double> valueDisplay) {
		this.valueDisplay = valueDisplay;
		repaint();
	}

	/**
	 * Nastavi aktualnu hodnotu, ktora ma byt zobrazena. Ak je rozsah 90 jednotiek, tak parameter mensi ako min nastavi
	 * hodnotu na min, parameter vacsi ako max nastavi hodnotu na max. Ak je rozsah 360 jednotiek, kontrola sa nevykona,
	 * rucicka zobrazuje hodnotu X rovnako ako X + 360.
	 *
	 * @param value nova hodnota, ktora sa ma zobrazit
	 */
	public void setValue(double value) {
		value -= min;

		if (max - min == 90) {
			if (value < 0) {
				value = 0;
			}

			if (value > 90) {
				value = 90;
			}
		}

		value = Math.round(value);

		this.direction = value;
		repaint();

		if (valueDisplay != null) {
			valueDisplay.accept(value + min);
		}
	}

	/** Aktualna hodnota zobrazovana na CanvasArrow. */
	public double getValue() {
		return direction + min;
	}

	/**
	 * Vypocita poziciu bodu v rovine.
	 *
	 * @param distance  vzdialenost od bodu (0, 0)
	 * @param direction uhol zovrety s kladnou osou x v stupnoch
	 * @return suradnice daneho bodu
	 */
	private Point sinCos(double distance, double direction) {
		int x = (int) (Math.cos((90 - direction) * Math.PI / 180) * distance);
		int y = (int) (Math.sin((-90 - direction) * Math.PI / 180) * distance);
		return new Point(x, y);
	}

	/**
	 * Nakreslenie komponentu, ak je rozsah 360 jednotiek. Vysledkom je kruhovy cifernik s ukazovatelom aktualnej
	 * hodnoty.
	 *
	 * @param g miesto, kam sa ma nakreslit tento komponent
	 */
	private void paint360(Graphics g) {
		Dimension size = getSize();
		int scale = Math.min(size.width, size.height) - 2;

		g.translate(size.width / 2, size.height / 2);

		if (isEnabled()) {
			g.setColor(Color.WHITE);
			g.fillOval(-scale / 2, -scale / 2, scale, scale);

			Point a = sinCos(0.42 * scale, direction);
			Point b = sinCos(0.06 * scale, direction + 90);

			g.setColor(ARROW_COLOR);
			g.fillPolygon(new int[] { a.x, b.x, -b.x }, new int[] { a.y, b.y, -b.y }, 3);
		}

		g.setColor(Color.BLACK);
		g.drawOval(-scale / 2, -scale / 2, scale, scale);

		for (int i = 0; i < 8; i++) {
			Point inner = sinCos(0.50 * scale, 22.5 * i);
			Point outer = sinCos(0.45 * scale, 22.5 * i);
			g.drawLine(inner.x, inner.y, outer.x, outer.y);
			g.drawLine(-inner.x, -inner.y, -outer.x, -outer.y);
		}
	}

	/**
	 * Nakreslenie komponentu, ak je rozsah 90 jednotiek. Vysledkom je stvrtkruhovy cifernik s ukazovatelom aktualnej
	 * hodnoty.
	 *
	 * @param g miesto, kam sa ma nakreslit tento komponent
	 */
	private void paint90(Graphics g) {
		Dimension size = getSize();
		int scale = Math.min(size.width, size.height) - 1;

		g.translate((size.width - scale) / 2, (size.height + scale) / 2);

		if (isEnabled()) {
			g.setColor(Color.WHITE);
			g.fillArc(-scale, -scale, 2 * scale, 2 * scale, 0, 90);

			Point a = sinCos(0.85 * scale, 90 - direction);
			Point b = sinCos(0.12 * scale, 180 - direction);

			g.setColor(ARROW_COLOR);
			g.fillPolygon(new int[] { a.x, b.x, -b.x }, new int[] { a.y, b.y, -b.y }, 3);
		}

		g.setColor(Color.BLACK);
		g.drawArc(-scale, -scale, 2 * scale, 2 * scale, 0, 90);
		g.drawLine(0, 0, 0, -scale);
		g.drawLine(0, 0, scale, 0);

		for (int i = 1; i < 4; i++) {
			Point inner = sinCos(scale, 22.5 * i);
			Point outer = sinCos(0.9 * scale, 22.5 * i);
			g.drawLine(inner.x, inner.y, outer.x, outer.y);
		}
	}

	@Override
	public void paint(Graphics g) {
		if (max - min == 90) {
			paint90(g);
		} else {
			paint360(g);
		}
	}

	@Override
	public Dimension getMinimumSize() {
		return new Dimension(100, 100);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(100, 100);
	}

}
