package sk.tekel04.scorched.windows;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JProgressBar;
import javax.swing.ListCellRenderer;

import sk.tekel04.scorched.arrow.PanelArrow;
import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.logging.Log;
import sk.tekel04.scorched.weapon.UserMissileFactory;

/**
 * Okno s ovladacimi prvkami pre hraca pocas hry. Ovlada vybrany tank, nastavuje jeho silu, uhol, zbran, zobrazuje
 * zivoty.
 */
public class GameControlsDialog extends JDialog {
	private static final long serialVersionUID = 3857788417204735071L;

	/** Komponent zobrazujuci otocenie zvoleneho tanku. */
	private PanelArrow direction;

	/** Komponent zobrazujuci uhol kanonu zvoleneho tanku. */
	private PanelArrow angle;

	/** Komponent zobrazujuci silu vystrelu zvoleneho tanku. */
	private PanelArrow power;

	/** Komponent umoznujuci vyber typu strely zvoleneho tanku. */
	private JComboBox<UserMissileFactory> comboBoxWeapon;

	/** Komponent zobrazujuci, ktory hrac je aktualne na tahu. */
	private JLabel labelPlayer;

	/** Komponent zobrazujuci pocet zivotov zvoleneho tanku. */
	private JProgressBar life;

	/** Tlacidlo na vystrelenie zvoleneho tanku */
	private JButton buttonFire;

	/** Vytvori okno s menu pre hraca. */
	public void init(Game game) {
		Log.entering("GameControlsDialog", "init");

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		setLayout(new GridBagLayout());

		labelPlayer = new JLabel("", JLabel.CENTER);
		add(labelPlayer, Position(0));

		direction = new PanelArrow("Direction", 0, 360);
		direction.setValueSuffix("°");
		add(direction, Position(1));

		angle = new PanelArrow("Angle", 0, 90);
		angle.setValueSuffix("°");
		add(angle, Position(2));

		power = new PanelArrow("Power", 10, 100);
		add(power, Position(3));

		life = new JProgressBar(0, 100);
		life.setStringPainted(true);
		add(life, Position(4));

		comboBoxWeapon = new JComboBox<>();
		UserMissileFactory.PLAYABLE_MISSILES.forEach(mf -> comboBoxWeapon.addItem(mf));
		ComboBoxWeaponRenderer renderer = new ComboBoxWeaponRenderer();
		renderer.setPreferredSize(new Dimension(200, 48));
		comboBoxWeapon.setRenderer(renderer);
		add(comboBoxWeapon, Position(5));

		buttonFire = new JButton("Fire!");
		buttonFire
		.addActionListener(e -> game.selectedTankShoot((UserMissileFactory) comboBoxWeapon.getSelectedItem()));
		add(buttonFire, Position(6));

		setEnabled(false);

		setTitle("Game Commands");
		setAlwaysOnTop(true);
		setLocation(0, 30);
		setSize(200, 570);
		setResizable(false);

		Log.exiting("GameControlsDialog", "init");
	}

	/**
	 * Pomocna funkcia na ulozenie komponentu na GridBagLayout. Komponenty budu ulozene v zvislom rade. * Kazdy
	 * komponent je natiahnuty na rovnaku sirku, vyska komponentov zostava povodna.
	 *
	 * @param y zvisla pozicia komponentu
	 * @return pozicia, na ktoru ulozit komponent
	 */
	private GridBagConstraints Position(int y) {
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;

		c.gridx = 0;
		c.gridy = y;
		return c;
	}

	/**
	 * Vracia pouzivatelom nastaveny smer otocenia tanku.
	 *
	 * @uml.property name="direction"
	 */
	public double getDirection() {
		return direction.getValue();
	}

	/**
	 * Vracia pouzivatelom nastaveny uhol kanonu tanku.
	 *
	 * @uml.property name="angle"
	 */
	public double getAngle() {
		return angle.getValue();
	}

	/**
	 * Vracia pouzivatelom nastavenu silu vystrelu tanku.
	 *
	 * @uml.property name="power"
	 */
	public double getPower() {
		return power.getValue();
	}

	/** Vracia pouzivatelom zvolenu zbran. */
	public int getWeapon() {
		return comboBoxWeapon.getSelectedIndex();
	}

	/**
	 * Zobrazi stav pouzivatelom zvoleneho tanku.
	 *
	 * @param direction smer otocenia
	 * @param angle     uhol kanonu
	 * @param power     sila vystrelu
	 * @param life      pocet zivotov
	 * @param weapon    zvolena zbran
	 */
	public void setValues(double direction, double angle, double power, double life, int weapon) {
		this.direction.setValue(direction);
		this.angle.setValue(angle);
		this.power.setValue(power);
		this.life.setValue((int) Math.max(1, Math.min(100, Math.round(life))));
		comboBoxWeapon.setSelectedIndex(weapon);
	}

	/**
	 * Vypne alebo zapne moznost menit udaje v okne. Okno je stale mozne pohybovat a menit jeho velkost.
	 *
	 * @param enabled <code>true</code> ak pouzivatel moze menit udaje
	 */
	@Override
	public void setEnabled(boolean enabled) {
		direction.setEnabled(enabled);
		angle.setEnabled(enabled);
		power.setEnabled(enabled);
		buttonFire.setEnabled(enabled);
		comboBoxWeapon.setEnabled(enabled);
	}

	/**
	 * Nastavi napis zobrazujuci, ktory hrac je na tahu.
	 *
	 * @param text  novy text
	 * @param color farba textu
	 */
	public void setPlayer(String text, Color color) {
		labelPlayer.setText(text + " player's turn");
		labelPlayer.setForeground(color);
	}

}

class ComboBoxWeaponRenderer extends JLabel implements ListCellRenderer<UserMissileFactory> {
	private static final long serialVersionUID = 7856152760709890129L;

	public ComboBoxWeaponRenderer() {
		setOpaque(true);
		setHorizontalAlignment(LEFT);
		setVerticalAlignment(CENTER);
	}

	/**
	 * This method finds the image and text corresponding to the selected value and returns the label, set up to display
	 * the text and image.
	 */
	@Override
	public Component getListCellRendererComponent(JList<? extends UserMissileFactory> list, UserMissileFactory value,
			int index, boolean isSelected, boolean cellHasFocus) {
		setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
		setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());

		// "index" refers to item over which the mouse is if it is open; -1 if it is closed.
		// "value" refers to item over which the mouse is if it is open; selected item if it is closed.
		setIcon(value.getImageIcon());
		setFont(list.getFont());
		setText(value.getHtmlDisplayName());
		setToolTipText(value.getDescription());

		return this;
	}

}
