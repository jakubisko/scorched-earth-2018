package sk.tekel04.scorched.windows;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.game.Tank;
import sk.tekel04.scorched.game.WrongGameSettingsException;
import sk.tekel04.scorched.logging.Log;

/**
 * Menu umoznujuce vytvorenie novej hry. Pozuivatel ma moznost zadat pocet hracov, pocet tankov pre kazdeho hraca,
 * otvorit okno na vytvorenie krajiny, nacitat krajinu zo suboru, zacat hru alebo skoncit program.
 */
public class FrameMainMenu extends JFrame {
	private static final long serialVersionUID = 4125160960423333481L;

	/** Objekt hry. */
	private Game game;

	/** Pouzivatelom nastaveny pocet hracov v hre. */
	private int numberPlayers = 2;

	/** Pouzivatelom nastaveny pocet tankov pre kazdeho hraca v hre. */
	private int numberTanks = 5;

	/** Akcia, ktorou sa nastartuje hra. */
	private Runnable startGame;

	/** Tlacidlo, ktorym sa nastartuje hra. */
	private JButton buttonStart;

	/** Vytvori vsetky komponenty na paneli. */
	public void init(Game game, Runnable startGame, Runnable showLandGenerator) {
		Log.entering("FrameMainMenu", "init");

		this.game = game;
		this.startGame = startGame;

		setTitle("Scorched Earth");
		setSize(400, 360);
		setLocationRelativeTo(null);
		setResizable(false);

		// When this window is closed, end program
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Log.info("Closing program");
			}
		});

		setLayout(new GridBagLayout());

		add(new JLabel("New Game", JLabel.CENTER), position(1, 0, 1));
		add(new JLabel("Players", JLabel.CENTER), position(0, 1, 1));
		add(new JLabel("Tanks", JLabel.CENTER), position(0, 2, 1));

		JButton buttonGenerate = new JButton("Random Land");
		buttonGenerate.addActionListener(e -> showLandGenerator.run());
		add(buttonGenerate, position(0, 3, 1));

		JButton buttonLoad = new JButton("Load Land");
		buttonLoad.addActionListener(e -> loadMapFromFile());
		add(buttonLoad, position(2, 3, 1));

		buttonStart = new JButton("Start game");
		buttonStart.addActionListener(e -> newGame());
		add(buttonStart, position(0, 4, 1));

		JButton buttonExit = new JButton("Exit");
		buttonExit.addActionListener(e -> {
			Log.info("Closing program via Exit button");
			System.exit(0);
		});
		add(buttonExit, position(2, 4, 1));

		JSlider sliderPlayers = new JSlider(JSlider.HORIZONTAL, 2, Tank.MAX_PLAYERS, numberPlayers);
		sliderPlayers.setMajorTickSpacing(1);
		sliderPlayers.setPaintTicks(true);
		sliderPlayers.setPaintLabels(true);
		sliderPlayers.setSnapToTicks(true);
		sliderPlayers.addChangeListener(e -> numberPlayers = sliderPlayers.getValue());
		add(sliderPlayers, position(1, 1, 2));

		JSlider sliderTanks = new JSlider(JSlider.HORIZONTAL, 1, 15, numberTanks);
		sliderTanks.setMajorTickSpacing(2);
		sliderTanks.setMinorTickSpacing(1);
		sliderTanks.setPaintTicks(true);
		sliderTanks.setPaintLabels(true);
		sliderTanks.setSnapToTicks(true);
		sliderTanks.addChangeListener(e -> numberTanks = sliderTanks.getValue());
		add(sliderTanks, position(1, 2, 2));

		Log.exiting("FrameMainMenu", "init");
	}

	/**
	 * Pomocna funkcia na ulozenie komponentu na GridBagLayout. Vyska komponentu zostava povodna.
	 *
	 * @param x lava suradnica komponentu
	 * @param y horna suradnica komponentu
	 * @param width sirka komponentu
	 * @return pozicia, na ktoru ulozit komponent
	 */
	private GridBagConstraints position(int x, int y, int width) {
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 0.2;

		c.gridx = x;
		c.gridy = y;
		c.gridwidth = width;
		return c;
	}

	/**
	 * Vytvori novu hru. Ak hru nie je mozne vytvorit (napr. nebola vytvorena krajina), zobrazi pouzivatelovi spravu
	 * oznacujucu chybu. Inak skryje okno menu a zobrazi okno s hrou.
	 */
	private void newGame() {
		try {
			game.newGame(numberPlayers, numberTanks);
			startGame.run();
		} catch (WrongGameSettingsException ex) {
			showError(ex.getMessage());
		} catch (Exception ex) {
			Log.exception("Failed to start new game", ex);
		}
	}

	/**
	 * Zobrazi pouzivatelovi dialog na vyber suboru, z ktoreho nacita krajinu. Ak je subor v nespravnom formate, zobrazi
	 * pouzivatelovi spravu.
	 */
	private void loadMapFromFile() {
		try {
			JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
			if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				game.loadLandFromFile(fc.getSelectedFile().toPath());
				buttonStart.setEnabled(true);
			}
		} catch (Exception ex) {
			showError("Wrong format of input file!");
			Log.exception("Failed to load a map", ex);
		}
	}

	/** Zobrazi pouzivatelovi okno s danou spravou. */
	private void showError(String text) {
		JOptionPane.showMessageDialog(this, text, "Error", JOptionPane.ERROR_MESSAGE);
		Log.info("User error: " + text);
	}

	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		if (b) {
			buttonStart.setEnabled(game.getLandMeter().getFieldSize() > 0);
		}
	}

}
