package sk.tekel04.scorched.windows;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JOptionPane;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.logging.Log;

/** Entry point to the app. */
public class Main {

	final Game game = new Game();
	final FrameMainMenu frameMainMenu = new FrameMainMenu();
	final FrameLandGenerator frameLandGenerator = new FrameLandGenerator();
	final FrameOpenGL frameOpenGL = new FrameOpenGL();
	final GameControlsDialog gameControlsDialog = new GameControlsDialog();

	private void init() {
		game.init(gameControlsDialog, this::endGameWithMessage);
		frameMainMenu.init(game, this::showGame, this::goToLandGenerator);
		frameLandGenerator.init(game, this::goToNewGame);
		frameOpenGL.init(game);
		gameControlsDialog.init(game);

		// When game window is maximised / minimised, show / hide dialog with game controls.
		frameOpenGL.addWindowStateListener(
				e -> gameControlsDialog.setVisible((e.getNewState() & Frame.ICONIFIED) != Frame.ICONIFIED));

		// When game window is closed, open main menu.
		frameOpenGL.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Log.info("User manually closed the game window");
				endGameWithoutMessage();
			}
		});

		// Open main menu
		frameMainMenu.setVisible(true);
	}

	/** Spusti hru. Zobrazi okno s grafikou hry. Skryje hlavne menu. */
	private void showGame() {
		frameMainMenu.setVisible(false);
		gameControlsDialog.setVisible(true);
		frameOpenGL.setVisible(true);
		game.setGameStopped(false);

		Log.info("New game began");
	}

	/** Zastavi hru. Skryje okno s grafikou hry. Zobrazi hlavne menu. */
	private void endGameWithoutMessage() {
		game.setGameStopped(true);
		game.clearLand();

		gameControlsDialog.setVisible(false);
		frameOpenGL.setVisible(false);
		frameMainMenu.setVisible(true);
	}

	/** Prepne hlavne menu do formularu na generovanie krajiny. */
	private void goToLandGenerator() {
		frameMainMenu.setVisible(false);
		frameLandGenerator.setVisible(true);
	}

	/** Prepne hlavne menu formularu na vytvorenie novej hry. */
	private void goToNewGame() {
		frameMainMenu.setVisible(true);
		frameLandGenerator.setVisible(false);
	}

	/**
	 * Ukonci prebiehajucu hru. Zavrie okno s grafikou a otvori hlavne menu. Zobrazi pouzivatelovi spravu o vitazovi
	 * hry. Vymaze krajinu, na ktorej sa hra odohravala.
	 *
	 * @param message Sprava pre pouzivatela o vitazovi hry
	 */
	private void endGameWithMessage(String message) {
		endGameWithoutMessage();
		JOptionPane.showMessageDialog(frameMainMenu, message, "Results", JOptionPane.INFORMATION_MESSAGE);

		Log.info(message);
	}

	public static void main(String[] args) {
		Log.info("Starting program");

		try {
			new Main().init();
		} catch (Throwable t) {
			Log.exception("Program initialization failed", t);
		}
	}

}
