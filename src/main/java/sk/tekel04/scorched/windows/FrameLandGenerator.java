package sk.tekel04.scorched.windows;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.logging.Log;

/** Okno, v ktorom pouzivatel nastavi parametre pre vytvorenie novej nahodnej krajiny. */
public class FrameLandGenerator extends JFrame {
	private static final long serialVersionUID = 6615142922634679684L;

	/** Defaultna velkost krajiny. */
	private static final int INITIAL_SIZE = 75;

	/** Defaultna velkost kopcov. */
	private static final int INITIAL_HILLS = 100;

	/** Defaultne percento krajiny pokryte vodou. */
	private static final int INITIAL_WATER = 50;

	/** Vytvori panel s komponentami. Komponenty ovladaju nastavenia pre vytvorenie nahodnej mapy. */
	public void init(Game game, Runnable backToMainMenu) {
		Log.entering("FrameLandGenerator", "init");

		setTitle("Scorched Earth");
		setSize(400, 360);
		setLocationRelativeTo(null);
		setResizable(false);

		// When this window is closed, go back to main menu
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				backToMainMenu.run();
			}
		});

		setLayout(new GridBagLayout());

		add(new JLabel("Create Random Land", JLabel.CENTER), position(0, 0, 2));
		add(new JLabel("Size", JLabel.CENTER), position(0, 1, 1));
		add(new JLabel("Hills", JLabel.CENTER), position(0, 3, 1));
		add(new JLabel("Water", JLabel.CENTER), position(0, 5, 1));

		JLabel labelSize = new JLabel(String.valueOf(INITIAL_SIZE), JLabel.CENTER);
		add(labelSize, position(1, 1, 1));

		JLabel labelHills = new JLabel(String.valueOf(INITIAL_HILLS), JLabel.CENTER);
		add(labelHills, position(1, 3, 1));

		JLabel labelWater = new JLabel(String.valueOf(INITIAL_WATER), JLabel.CENTER);
		add(labelWater, position(1, 5, 1));

		JSlider sliderSize = new JSlider(JSlider.HORIZONTAL, 25, 150, INITIAL_SIZE);
		sliderSize.setMajorTickSpacing(25);
		sliderSize.setMinorTickSpacing(5);
		sliderSize.setPaintTicks(true);
		sliderSize.setPaintLabels(true);
		sliderSize.addChangeListener(e -> labelSize.setText(String.valueOf(sliderSize.getValue())));
		add(sliderSize, position(0, 2, 2));

		JSlider sliderHills = new JSlider(JSlider.HORIZONTAL, 25, 200, INITIAL_HILLS);
		sliderHills.setMajorTickSpacing(25);
		sliderHills.setMinorTickSpacing(5);
		sliderHills.setPaintTicks(true);
		sliderHills.setPaintLabels(true);
		sliderHills.addChangeListener(e -> labelHills.setText(String.valueOf(sliderHills.getValue())));
		add(sliderHills, position(0, 4, 2));

		JSlider sliderWater = new JSlider(JSlider.HORIZONTAL, 10, 90, INITIAL_WATER);
		sliderWater.setMajorTickSpacing(20);
		sliderWater.setMinorTickSpacing(5);
		sliderWater.setPaintTicks(true);
		sliderWater.setPaintLabels(true);
		sliderWater.addChangeListener(e -> labelWater.setText(String.valueOf(sliderWater.getValue())));
		add(sliderWater, position(0, 6, 2));

		JButton buttonCancel = new JButton("Cancel");
		buttonCancel.addActionListener(e -> backToMainMenu.run());
		add(buttonCancel, position(1, 7, 1));

		JButton buttonCreate = new JButton("Create");
		buttonCreate.addActionListener(e -> {
			game.createRandomLand(sliderSize.getValue(), sliderHills.getValue(), sliderWater.getValue());
			backToMainMenu.run();
		});
		add(buttonCreate, position(0, 7, 1));

		Log.exiting("FrameLandGenerator", "init");
	}

	/**
	 * Pomocna funkcia na ulozenie komponentu na GridBagLayout. Vyska komponentu zostava povodna.
	 *
	 * @param x lava suradnica komponentu
	 * @param y horna suradnica komponentu
	 * @param width sirka komponentu
	 * @return pozicia, na ktoru ulozit komponent
	 */
	private GridBagConstraints position(int x, int y, int width) {
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 0.2;

		c.gridx = x;
		c.gridy = y;
		c.gridwidth = width;
		return c;
	}

}
