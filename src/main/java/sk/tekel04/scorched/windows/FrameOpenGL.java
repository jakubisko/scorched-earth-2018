package sk.tekel04.scorched.windows;

import java.util.function.IntConsumer;

import javax.swing.JFrame;

import com.jogamp.opengl.awt.GLCanvas;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.game.GraphicsListener;
import sk.tekel04.scorched.game.MovementListener;
import sk.tekel04.scorched.logging.Log;

/** Okno, v ktorom sa zobrazuje 3D graficka cast hry. */
public class FrameOpenGL extends JFrame {
	private static final long serialVersionUID = -4084852096136885155L;

	/** OpenGL plocha, na ktoru sa kresli. */
	private GLCanvas canvas;

	/**
	 * Vytvori nove okno s 3D grafikou pre danu hru. Vytvori aj GraphicsListener a MovementListener, ktore sa staraju o
	 * vsetky pouzivatelske vstupy v tomto okne.
	 */
	public void init(Game game) {
		Log.entering("FrameOpenGL", "init");

		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		IntConsumer fpsDisplay = fps -> setTitle("Scorched Earth [" + fps + " fps]");
		MovementListener mListener = new MovementListener(game.getLandMeter());
		GraphicsListener gListener = new GraphicsListener(mListener, game, fpsDisplay);

		canvas = new GLCanvas();
		canvas.addGLEventListener(gListener);
		canvas.addKeyListener(mListener);
		canvas.addMouseListener(mListener);
		canvas.addMouseWheelListener(mListener);
		canvas.addMouseMotionListener(mListener);
		add(canvas);

		setSize(565, 400);
		setLocation(200, 0);
		setExtendedState(JFrame.MAXIMIZED_BOTH);

		Log.exiting("FrameOpenGL", "init");
	}

	/** Zobrazi okno s grafikou a da mu focus, aby sa dalo ovladat klavesnicou. */
	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		canvas.requestFocus();
	}

}
