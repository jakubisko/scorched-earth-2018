package sk.tekel04.scorched.picture;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.function.Consumer;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.logging.Log;

/**
 * Jedna suradnica v priestore. Jej hodnota je reprezentovana vzdialenostou od bodu (0, 0, 0), a smerom, ktorym je tato
 * vzdialenost pocitana. Smer musi byt zhodny s orientaciou jednej z osi.
 */
class Coordinate {

	/** Orientacia v smere osi x. */
	public static final int X_SUR = 1;

	/** Orientacia v smere osi y. */
	public static final int Y_SUR = 2;

	/** Orientacia v smere osi z. */
	public static final int Z_SUR = 3;

	/** Vzdialenost od bodu (0, 0, 0) */
	public final double value;

	/** Orientacia v priestore. */
	public final int orientation;

	/**
	 * Konstruktor nastavi smer a vzdialenost suradnice.
	 *
	 * @param orientation orientacia v priestore
	 * @param value vzdialenost od bodu (0, 0, 0)
	 * @throws IllegalArgumentException ak orientacia nie je zhodna s jednou z konstant reprezentujucich orientaciu osi
	 */
	public Coordinate(int orientation, double value) {
		if (orientation != X_SUR && orientation != Y_SUR && orientation != Z_SUR) {
			throw new IllegalArgumentException();
		}

		this.value = value;
		this.orientation = orientation;
	}
}

/**
 * Jeden vrchol v priestore. Moze obsahovat aj bod v dvojrozmernom priestore, ktory urcuje prisluchajuce miesto na
 * texture. Na zostrojenie kocky je teda potrebnych 8 objektov, na zostrojenie kocky s texturami treba 24 objektov (1 na
 * kazdy vrchol kazdej steny).
 */
class Vertex {

	/** Pozicia v priestore. */
	public final double x;
	/** Pozicia v priestore. */
	public final double y;
	/** Pozicia v priestore. */
	public final double z;

	/** Miesto na texture. */
	public final double u;
	/** Miesto na texture. */
	public final double v;

	/**
	 * Vytvori novy bod bez textury.
	 *
	 * @param x x-suradnica v priestore
	 * @param y y-suradnica v priestore
	 * @param z z-suradnica v priestore
	 */
	public Vertex(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.u = 0;
		this.v = 0;
	}

	/**
	 * Vytvori novy bod aj s urcenim pozicie na texture.
	 *
	 * @param b {@link Vertex} urcujuci bod v priestore
	 * @param u x-suradnica miesta na texture
	 * @param v y-suradnica miesta na texture
	 */
	public Vertex(Vertex b, double u, double v) {
		this.x = b.x;
		this.y = b.y;
		this.z = b.z;
		this.u = u;
		this.v = v;
	}
}

/**
 * Stena so 4 hranami v priestore. Vrcholy sa musia nachadzat v jednej rovine, pretoze normalovy vektor je spolocny pre
 * celu stenu.
 */
class Surface implements UnselectablePicture {

	/**
	 * Vrcholy steny.
	 *
	 * @uml.property name="vertex"
	 * @uml.associationEnd multiplicity="(0 -1)"
	 */
	private final Vertex vertex[] = new Vertex[4];

	/** Zlozka normaloveho vektoru steny. */
	private final double n_x;
	/** Zlozka normaloveho vektoru steny. */
	private final double n_y;
	/** Zlozka normaloveho vektoru steny. */
	private final double n_z;

	/**
	 * Vytvori novu stenu zadanu 4 vrcholmi.
	 *
	 * @param v pole 4 vrcholov
	 * @throws IllegalArgumentException ak vstupne pole nema 4 prvky
	 */
	public Surface(Vertex v[]) {
		if (v.length != 4) {
			throw new IllegalArgumentException();
		}

		for (int i = 0; i < 4; i++) {
			vertex[i] = v[i];
		}

		double u_x = vertex[1].x - vertex[0].x;
		double u_y = vertex[1].y - vertex[0].y;
		double u_z = vertex[1].z - vertex[0].z;

		double v_x = vertex[2].x - vertex[0].x;
		double v_y = vertex[2].y - vertex[0].y;
		double v_z = vertex[2].z - vertex[0].z;

		double m_x = u_y * v_z - u_z * v_y;
		double m_y = u_z * v_x - u_x * v_z;
		double m_z = u_x * v_y - u_y * v_x;

		double d = Math.sqrt(m_x * m_x + m_y * m_y + m_z * m_z);

		n_x = m_x / d;
		n_y = m_y / d;
		n_z = m_z / d;
	}

	/**
	 * Nakresli stenu. Stena je skonstruovana z dvoch trojuholnikov. Pre vrcholy je vygenerovana pozicia na texture, ale
	 * tato trieda nedefinuje ziadnu texturu, ktora by mala byt pouzita. Preto bude pouzita posledna textura.
	 */
	@Override
	public void draw(GL2 gl) {
		gl.glNormal3d(n_x, n_y, n_z);

		for (int i = 0; i < 4; i++) {
			gl.glTexCoord2d(vertex[i].u, vertex[i].v);
			gl.glVertex3d(vertex[i].x, vertex[i].y, vertex[i].z);
		}
	}

}

/**
 * Trojrozmerny model poskladany z mnoziny 4 vrcholovych stien. Model sa nacitava zo suborov. Kazdy model je definovany
 * 3 subormi. Tie popisuju suradnice, vrcholy a steny modelu. Tieto subory sa nachadzaju v spolocnom adresari; jeho
 * nazov definuje nazov modelu.
 */
public final class Model implements UnselectablePicture {

	/** Zoznam stien, z ktorych sa sklada model. */
	private List<Surface> surfaces = new ArrayList<>();

	/** Textura modelu. */
	private final int texture;

	/** Nakresli model do stredu suradnicoveho systemu. Model bude pokryty svojou texturou. */
	@Override
	public void draw(GL2 gl) {
		gl.glBindTexture(GL2.GL_TEXTURE_2D, texture);

		gl.glBegin(GL2.GL_QUADS);
		for (Surface surface : surfaces) {
			surface.draw(gl);
		}
		gl.glEnd();
	}

	/**
	 * Vytvori novy model z resource daneho mena.
	 *
	 * @param textureFileName nazov suboru s texturou
	 * @param modelName nazov adresaru s modelom
	 */
	public Model(GL2 gl, String textureFileName, String modelName) {

		// Tabulka suradnic, z ktorych sa skladaju vrcholy modelu.
		Hashtable<String, Vertex> vertexes = new Hashtable<>();

		// Tabulka vrcholov, z ktorych sa skladaju steny modelu.
		Hashtable<String, Coordinate> coordinates = new Hashtable<>();

		/** Pomocna trieda na nacitanie a skonstruovanie modelu. */
		class ModelLoader {

			/** Adresar s modelmi. */
			private static final String MODEL_PATH = "/textures/";

			/**
			 * Spracuje jeden riadok zo suboru obsahujuceho suradnice. Nazov suradnice je vlozeny do hash tabulky ako
			 * kluc, suradnica je vlozena ako hodnota. Vdaka tomu sa moze na tuto suradnicu odvolavat vrchol jej menom.
			 *
			 * @param words slova jedneho riadku v subore
			 */
			private void loadCoordinate(String[] words) {
				String id = words[0];

				int orientation = -1;
				if (words[1].equalsIgnoreCase("x")) {
					orientation = Coordinate.X_SUR;
				}
				if (words[1].equalsIgnoreCase("y")) {
					orientation = Coordinate.Y_SUR;
				}
				if (words[1].equalsIgnoreCase("z")) {
					orientation = Coordinate.Z_SUR;
				}

				double value = Double.parseDouble(words[2]);

				coordinates.put(id, new Coordinate(orientation, value));
			}

			/**
			 * Spracuje jeden riadok zo suboru obsahujuceho vrcholy. Nazov vrcholu je vlozeny do hash tabulky ako kluc,
			 * vrchol je vlozeny ako hodnota. Vdaka tomu sa moze na tento vrchol odvolavat stena jeho menom. Vrchol je
			 * definovany troma suradnicami, ktore musia byt kazda inej orientacie. Kedze si pamatame pre suradnice
			 * orientaciu, je uplne jedno, v akom poradi su suradnice v riadku.
			 *
			 * @param words slova jedneho riadku v subore
			 */
			private void loadVertex(String[] words) {
				String id = words[0];

				Coordinate a = coordinates.get(words[1]);
				Coordinate b = coordinates.get(words[2]);
				Coordinate c = coordinates.get(words[3]);

				if (a == null || b == null || c == null) {
					Log.warning("Following vertex has nonexisting coordinates: " + id);
				}

				if (a.orientation == b.orientation || a.orientation == c.orientation
						|| b.orientation == c.orientation) {
					Log.warning("Following vertex has repeating coordinates: " + id);
				}

				double x, y, z;

				if (a.orientation == Coordinate.X_SUR) {
					x = a.value;
				} else if (b.orientation == Coordinate.X_SUR) {
					x = b.value;
				} else {
					x = c.value;
				}

				if (a.orientation == Coordinate.Y_SUR) {
					y = a.value;
				} else if (b.orientation == Coordinate.Y_SUR) {
					y = b.value;
				} else {
					y = c.value;
				}

				if (a.orientation == Coordinate.Z_SUR) {
					z = a.value;
				} else if (b.orientation == Coordinate.Z_SUR) {
					z = b.value;
				} else {
					z = c.value;
				}

				vertexes.put(id, new Vertex(x, y, z));
			}

			/**
			 * Spracuje jeden riadok zo suboru obsahujuceho steny. Steny su vkladane do spajaneho zoznamu, cez ktory sa
			 * bude prechadzat vzdy, ked sa bude model vykreslovat. Popis steny sa sklada z nazvu 4 vrcholov, a z 2
			 * suradnic pre kazdy vrchol opisujucich poziciu na texture.
			 *
			 * @param words slova jedneho riadku v subore
			 */
			private void loadSurface(String[] words) {
				Vertex point[] = new Vertex[4];

				for (int i = 0; i < 4; i++) {
					double u = Double.parseDouble(words[4 * i + 1]);
					double v = Double.parseDouble(words[4 * i + 2]);
					point[i] = new Vertex(vertexes.get(words[4 * i]), u, v);
					if (point[i] == null) {
						Log.warning("Surface file contains nonexisting vertex: " + words[4 * i]);
					}
				}

				surfaces.add(new Surface(point));
			}

			/**
			 * Nacita jeden subor modelu a pre kazdy neprazdny riadok zavola danu akciu.
			 *
			 * @param resourceName nazov suboru, ktory sa nacitava
			 * @param lineConsumer akcia, ktora sa vykona pre kazdy riadok suboru
			 */
			private void processFile(String resourceName, Consumer<String[]> lineConsumer) {
				String fullName = MODEL_PATH + modelName + "/" + resourceName;
				try (BufferedReader reader = new BufferedReader(
						new InputStreamReader(getClass().getResourceAsStream(fullName), StandardCharsets.UTF_8))) {
					reader.lines().filter(line -> !line.equals(""))
					.forEach(line -> lineConsumer.accept(line.split("[ ]+")));
				} catch (IOException e) {
					Log.exception("Exception thrown while reading model file " + fullName, e);
				}
			}

		}

		ModelLoader modelLoader = new ModelLoader();
		modelLoader.processFile("coordinates.txt", line -> modelLoader.loadCoordinate(line));
		modelLoader.processFile("vertexes.txt", line -> modelLoader.loadVertex(line));
		modelLoader.processFile("surfaces.txt", line -> modelLoader.loadSurface(line));

		SimpleTextureLoader loader = new SimpleTextureLoader();
		texture = loader.loadTexture(gl, textureFileName);

		Log.info(modelName + " model successfully created");
	}

}
