package sk.tekel04.scorched.picture;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;

/** Pomocna trieda na kreslenie gule. */
public final class SphereDrawer {

	/** OpenGL Utility Library, poskytuje vyssie prikazy na pracu s OpenGL */
	private static final GLU glu = new GLU();

	/** Definuje povrch gule. */
	private static GLUquadric quadric;

	/** Nacita povrch gule. */
	public static void loadQuadric(GL2 gl) {
		quadric = glu.gluNewQuadric();
	}

	/**
	 * Nakresli gulu na danom mieste. Gula bude danej farby, netyka sa jej osvetlenie, cize nema ziaden tien. Gula
	 * nebude pokryta texturou. Farba gule je urcena polom 3 <code>double</code>, pricom tie predstavuju zlozky farby
	 * Red, Green, Blue. Hodnota kazdej zlozky sa pohybuje od 0 do 1 vratane.
	 *
	 * @param x x-suradnica stredu gule
	 * @param y y-suradnica stredu gule
	 * @param z z-suradnica stredu gule
	 * @param radius polomer gule
	 * @param resolution pocet poludnikov a rovnobeziek
	 * @param color pole urcujuce farbu
	 */
	public static void drawSphere(GL2 gl, double x, double y, double z, double radius, int resolution, double[] color) {
		gl.glPushMatrix();

		gl.glDisable(GL2.GL_LIGHTING);
		gl.glColor3d(color[0], color[1], color[2]);

		gl.glTranslated(x, y, z);
		gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);

		glu.gluQuadricNormals(quadric, GLU.GLU_NONE);
		glu.gluSphere(quadric, radius, resolution, resolution);

		gl.glColor3d(1.0, 1.0, 1.0);
		gl.glEnable(GL2.GL_LIGHTING);

		gl.glPopMatrix();
	}

}
