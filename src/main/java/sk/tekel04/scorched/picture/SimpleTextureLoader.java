package sk.tekel04.scorched.picture;

import java.util.Objects;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;

import sk.tekel04.scorched.logging.Log;

/** Poskytuje metodu na nacitanie textur. Je to len wrapper pre triedu {@link TextureLoader}. */
public final class SimpleTextureLoader {

	/** Lokacia textur v resource adresari. */
	private static final String TEXTURE_PATH = "/textures/";

	/** OpenGL Utility Library, poskytuje vyssie prikazy na pracu s OpenGL */
	private static final GLU glu = new GLU();

	/**
	 * Nacita texturu zo suboru alebo z resource.
	 *
	 * @param fileName nazov suboru s texturou
	 * @return identifikator textury, alebo -1 ak sa texturu nepodarilo nacitat
	 */
	public int loadTexture(GL2 gl, String fileName) {
		try {
			Objects.requireNonNull(gl, "Loading of textures before initializing OpenGL is impossible.");

			int result = TextureLoader.readTexture(TEXTURE_PATH + fileName).toGL(gl, glu, false);
			Log.info(fileName + " texture successfully loaded");

			return result;
		} catch (final Exception e) {
			Log.exception("Could not load texture " + fileName, e);
			return -1;
		}
	}

}
