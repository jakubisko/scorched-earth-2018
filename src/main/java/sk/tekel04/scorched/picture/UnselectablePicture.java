package sk.tekel04.scorched.picture;

import com.jogamp.opengl.GL2;

/** Trieda implementujuca toto rozhranie sa da nakreslit. Obrazok nie je mozne vybrat mysou. */
public interface UnselectablePicture {

	/**
	 * Nakresli objekt na obrazovku.
	 *
	 * @param select <code>true</code>, ak objekt ma navyse udavat aj svoj identifikator, aby bolo mozne urcit, ktory
	 *            objekt si pouzivatel vybral mysou
	 */
	abstract public void draw(GL2 gl);

}
