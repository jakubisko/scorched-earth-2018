package sk.tekel04.scorched.picture;

import com.jogamp.opengl.GL2;

/** Trieda implementujuca toto rozhranie sa da nakreslit. Tento obrazok si moze pouzivatel vybrat mysou. */
public interface SelectablePicture {

	/**
	 * Nakresli objekt na obrazovku.
	 *
	 * @param select <code>true</code>, ak objekt ma navyse udavat aj svoj identifikator, aby bolo mozne urcit, ktory
	 *            objekt si pouzivatel vybral mysou
	 */
	void draw(GL2 gl, boolean select);

}
