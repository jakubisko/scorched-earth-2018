package sk.tekel04.scorched.weapon;

import java.awt.Image;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import sk.tekel04.scorched.logging.Log;

/** Architektonicky vzor Factory na vytvaranie zbrani. */
public abstract class UserMissileFactory {

	/** Meno zbrane, ktore sa zobrazuje pouzivatelovi. */
	private final String htmlDisplayName;

	/** Tooltip pre pouzivatela popisujuci zbran. */
	private final String description;

	/** Ikonka zbrane, ktora sa zobrazuje pouzivatelovi. */
	private final ImageIcon imageIcon;

	protected UserMissileFactory(String displayName, String description, String imageIconName) {
		Image image = null;

		try {
			image = ImageIO.read(UserMissileFactory.class.getResource(imageIconName));
		} catch (IOException e) {
			Log.exception("Failed to load icon " + imageIconName, e);

		}

		this.htmlDisplayName = "<html>" + displayName + "</html>";
		this.description = description;
		imageIcon = new ImageIcon(image);
	}

	/** Vytvori novu instanciu zbrane, nastavi jej poziciu a rychlost pohybu. */
	public abstract void createInstance(double x, double y, double z, double direction, double angle, double power);

	/** Zoznam zbrani, ktore sa daju pouzit v hre. */
	public static final List<UserMissileFactory> PLAYABLE_MISSILES = Arrays.asList( //
			MissileAndiVehicle.getFactory(), //
			MissileWaterBounceFarmer.getFactory(), //
			MissileChiccy.getFactory(), //
			Missile3on.getFactory(), //
			MissileHolotovCoctail.getFactory(), //
			MissileShankiFireworks.getFactory(), //
			MissileMultiMaros.getFactory(), //
			PalinohaRiffle.getFactory(), //
			MissileLuVar.getFactory(), //
			MissileMapa.getFactory(), //
			MissileNoxious.getFactory(), //
			MissileFlurryFreezer.getFactory() //
			);

	public String getHtmlDisplayName() {
		return htmlDisplayName;
	}

	public String getDescription() {
		return description;
	}

	public ImageIcon getImageIcon() {
		return imageIcon;
	}

}
