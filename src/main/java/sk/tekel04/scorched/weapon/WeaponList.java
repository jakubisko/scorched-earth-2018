package sk.tekel04.scorched.weapon;

import java.util.LinkedList;
import java.util.ListIterator;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.UnselectablePicture;

/**
 * Zoznam vsetkych letiacich zbrani (striely, vybuchy, dym atd). Svojou funkcionalitou pripomina {@link LinkedList},
 * avsak vzhladom na specificke poziadavky ju nie je mozne implementovat s pouzitim len jedneho LinkedList. Objekty
 * prechadza jednym smerom v poradi, v akom boli do zoznamu pridavane. Nove objekty je mozne pridavat pocas prechadzania
 * cez zoznam. Nove objekty sa pridaju na koniec zoznamu, ale az pred zaciatkom dalsieho prechadzania (tato vlastnost je
 * nutna vzhladom na sposob implementacie zbrani). Zoznam je prazdny vtedy, ked je prazdny zoznam prechadzanych objektov
 * aj zoznam novo pridanych objektov. Jednotlive objekty zoznamu sa pri prechadzani vykresluju.
 */
public class WeaponList implements UnselectablePicture {

	/** Stare objekty v zozname. */
	private LinkedList<AbstractWeapon> listOld = new LinkedList<AbstractWeapon>();

	/** Nove objekty, ktore sa pridaju na koniec zoznamu pred zaciatkom dalsieho prechadzania. */
	private LinkedList<AbstractWeapon> listNew = new LinkedList<AbstractWeapon>();

	/** Prejde vsetky objekty v zozname a nakresli ich. Znicene zbrane budu zo zoznamu vylucene. */
	@Override
	public void draw(GL2 gl) {
		if (!listNew.isEmpty()) {
			listOld.addAll(listNew);
			listNew.clear();
		}

		ListIterator<AbstractWeapon> iterator = listOld.listIterator();
		while (iterator.hasNext()) {
			AbstractWeapon weapon = iterator.next();

			weapon.move();

			if (weapon.isDestroyed()) {
				iterator.remove();
			} else {
				weapon.draw(gl);
			}
		}
	}

	/**
	 * Prida novu zbran do zoznamu.
	 *
	 * @param weapon zbran, ktoru pridat
	 */
	public void add(AbstractWeapon weapon) {
		listNew.add(weapon);
	}

	/** Vrati, ci je zoznam prazdny. */
	public boolean isEmpty() {
		return listOld.isEmpty() && listNew.isEmpty();
	}

	/** Zmaze vsetky prvky zoznamu. */
	public void clear() {
		listOld.clear();
		listNew.clear();
	}

}
