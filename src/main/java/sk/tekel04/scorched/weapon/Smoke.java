package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.picture.SphereDrawer;

/**
 * Oblacik dymu. Dym stupa nepravidelnym pohybom smerom hore, pricom sa jeho objem zmensuje, az nakoniec uplne zmizne.
 * Dym nevplyva na priebeh hry.
 */
public class Smoke extends AbstractWeapon {

	/** Farba oblacika. */
	private double[] color = new double[3];

	/** Objem oblacika. */
	private double mass;

	/**
	 * Vytvori novy oblacik dymu na nahodnom mieste okolo daneho bodu. Oblacik bude mat farbu nahodneho odtienu sivej, a
	 * bude mat nahodnu velkost.
	 *
	 * @param x x-suradnica miesta na vytvorenie
	 * @param y y-suradnica miesta na vytvorenie
	 * @param z z-suradnica miesta na vytvorenie
	 * @param radius vzdialenost, o ktoru sa moze dym vytvorit vedla daneho bodu
	 */
	public Smoke(double x, double y, double z, double radius) {
		super(x + randomRange(radius), y + randomRange(radius), z + randomRange(radius));

		mass = 0.02 + 0.12 * Math.random();

		color[0] = color[1] = color[2] = Math.random();
	}

	/**
	 * Zmensi objem oblacika dymu, a posunie ho smerom hore. Oblacik dymu s nulovou hmotnostou je zniceny.
	 */
	@Override
	public void move() {
		if (!exists) {
			return;
		}

		mass -= 0.01 * Game.TIME;
		x += randomRange(0.2) * Game.TIME;
		y += (0.2 - mass) * Game.TIME;
		z += randomRange(0.2) * Game.TIME;

		exists = mass > 0;
	}

	/** Nakresli oblacik dymu. Dym je tvoreny gulou sedeho odtienu. */
	@Override
	public void draw(GL2 gl) {
		if (exists) {
			SphereDrawer.drawSphere(gl, x, y, z, mass, 6, color);
		}
	}

}
