package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.Model;

/**
 * Zbran, ktora vystreli 7 striel tvoriacich vejar. V skutocnosti tato zbran neexistuje, pretoze sa znici hned po svojom
 * vytvoreni. Vo svojom konstruktore vsak vytvori spominanych 7 mensich striel.
 */
public class MissileMultiMaros extends AbstractMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("Multi Maros<br>Anti-Kiribati Gun", "Spread shot",
				"/icons/MissileMultiMaros.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileMultiMaros(x, y, z, direction, angle, power);
			}
		};
	}

	/** Vytvori 7 mensich striel tvoriacich vejar. Ihned potom sa zbran sama znici. */
	private MissileMultiMaros(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
		exists = false;

		for (int i = -30; i <= 30; i += 10) {
			new LittleMultiMaros(x, y, z, direction + i, angle, power);
		}
	}

	@Override
	public void draw(GL2 gl) {
		// Tato zbran sa nekresli
	}

	public static void loadModel(GL2 gl) {
		LittleMultiMaros.loadModel(gl);
	}

}

/** Jedna strela vytvorena zbranou {@link MissileMultiMaros} . Standardna strela, ktora vybuchne po kolizii s kopcom. */
class LittleMultiMaros extends AbstractMissile {

	private static Model model;

	public LittleMultiMaros(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	@Override
	protected void collisionEarth() {
		explode(2.2, 99);
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileNormal");
	}

}
