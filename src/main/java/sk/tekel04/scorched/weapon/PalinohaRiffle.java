package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.picture.SphereDrawer;

/**
 * Gulomet s kratkym dostrelom. V skutocnosti objekt tejto triedy nevidno, iba generuje jednotlive strely. Po vystreleni
 * poslednej strely sa automaticky znici. Dostrel tejto zbrane je len polovicny oproti beznym zbraniam.
 */
public class PalinohaRiffle extends AbstractMovableWeapon {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("Pa-LIN-(0ha)<br>55mm Riffle", "Releases multiple short-range shots",
				"/icons/PalinohaRiffle.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new PalinohaRiffle(x, y, z, direction, angle, power);
			}
		};
	}

	/** Otocenie strielajuceho tanku. */
	private double direction;

	/** Uhol poslednej vystrelenej strely zovrety so zemou. */
	private double angle;

	/** Rychlost poslednej vystrelenej strely. */
	private double power;

	/**
	 * Pocitadlo zvysnych striel. Tato hodnota nemusi predstavovat skutocny pocet zostavajucich striel, pretoze strelba
	 * sa moze zastavit aj skor. Tank prestane strielat, ak by padali strely naspat na tento tank.
	 */
	private int remains = 12;

	/**
	 * Vytvori novu instanciu gulometu, a ulozi si nastavenia strelby zvolene pouzivatelom. Vytvori nastavenia, ktore
	 * bude mat prva vystrelena strela.
	 */
	private PalinohaRiffle(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);

		this.direction = direction;
		this.angle = Math.max(angle - 3 * remains / 2, -10);
		this.power = power / 2;
	}

	/**
	 * Vystreli novu strelu z gulometu a pozmeni smer, ktorym bude letiet dalsia strela. Ak sa minuli naboje, alebo by
	 * delo smerovalo dohora, zastavi palbu a znici sa.
	 */
	@Override
	public void move() {
		if (!exists) {
			return;
		}

		int remainsBefore = (int) Math.floor(remains);
		remains -= Game.TIME;
		int remainsAfter = (int) Math.floor(remains);

		for (int i = remainsAfter; exists && i < remainsBefore; i++) {
			new LittlePalinoha(x, y, z, direction, angle, power);

			angle += 3;
			power *= 1.04;

			exists = (remains > 0) && (angle < 85);
		}
	}

	@Override
	public void draw(GL2 gl) {
		// Tato zbran sa nekresli
	}

}

/** Jedna strela z gulometu. Pohybuje sa po balistickej krivke. Pri dopade nesposobuje vybuch. */
class LittlePalinoha extends AbstractMissile {
	private static final double[] COLOR = { 0.0, 0.0, 0.0 };

	public LittlePalinoha(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	@Override
	protected void collisionEarth() {
		damage(1.6, 16);
		super.collisionEarth();
	}

	@Override
	public void draw(GL2 gl) {
		if (exists) {
			SphereDrawer.drawSphere(gl, x, y, z, 0.05, 4, COLOR);
		}
	}

}
