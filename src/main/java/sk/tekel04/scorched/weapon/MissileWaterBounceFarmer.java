package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.Model;

/**
 * Strela, ktora sa odraza od vodnej hladiny. Pri kazdom naraze do vodnej hladiny nastane explozia, a strela pokracuje
 * dalej v pohybe. Uhol odrazu je mensi ako uhol dopadu, zbran sa teda postupne zacne hybat tesne nad hladinou. Idealna
 * zbran na zasahovanie cielov na malych ostrovoch. Odraza sa celkovo 12 krat.
 */
public class MissileWaterBounceFarmer extends AbstractMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("Water-Bounce<br>(Farmer) Shot", "Shell that bounces on water",
				"/icons/MissileWaterBounceFarmer.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileWaterBounceFarmer(x, y, z, direction, angle, power);
			}
		};
	}

	private static Model model;

	/** Pocitadlo zostavajucich odrazov. */
	private int bounceLeft = 12;

	private MissileWaterBounceFarmer(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	@Override
	protected void collisionEarth() {
		explode(2.55, 101);
	}

	/**
	 * Pri dopade na hladinu vybuchne. Znizi pocitadlo narazov. Ak este nie je hodnota rovna 0, tak obnovi zbran a zmeni
	 * jej smerovanie.
	 */
	@Override
	protected void collisionWater() {
		explode(2.55, 101);

		if (--bounceLeft > 0) {
			exists = true;

			speedX *= 1.1;
			speedY *= -0.6;
			speedZ *= 1.1;

			y = 0.1;
		}
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileNormal");
	}

}
