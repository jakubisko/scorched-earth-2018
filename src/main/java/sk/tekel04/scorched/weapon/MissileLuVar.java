package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.Model;

/** Standardna raketa dlheho dostrelu. Vybuchne po naraze do steny. */
public class MissileLuVar extends AbstractHighRangeMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("High-Range<br>LuVar Arrow", "Standard high-range projectile",
				"/icons/MissileLuVar.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileLuVar(x, y, z, direction, angle, power);
			}
		};
	}

	private static Model model;

	private MissileLuVar(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	@Override
	protected void collisionEarth() {
		explode(2.15, 147);
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileLong");
	}

}
