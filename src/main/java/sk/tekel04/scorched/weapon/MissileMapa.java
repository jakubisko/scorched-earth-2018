package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.Model;

/**
 * Raketa dlheho dostrelu. Velky rozmer vytvoreneho vybuchu, ale sposobuje nizke zranenia. Vybuchne aj po dopade na
 * vodnu hladinu.
 */
public class MissileMapa extends AbstractHighRangeMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("High-Range<br>M@P@ Rocket",
				"High-range shell with increased explosion diameter but decreased damage; explodes even on water",
				"/icons/MissileMapa.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileMapa(x, y, z, direction, angle, power);
			}
		};
	}

	private static Model model;

	private MissileMapa(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	@Override
	protected void collisionEarth() {
		explode(3.75, 68);
	}

	@Override
	protected void collisionWater() {
		explode(3.75, 68);
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileLong");
	}

}
