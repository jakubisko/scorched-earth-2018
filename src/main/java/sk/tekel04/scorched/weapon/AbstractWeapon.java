package sk.tekel04.scorched.weapon;

import sk.tekel04.scorched.game.Land;
import sk.tekel04.scorched.picture.UnselectablePicture;

/**
 * Zakladna trieda pre lubovolnu zbran v hre. Tiez zakladna trieda pre efekty suvisiace so zbranami - {@link Explosion}
 * a {@link Smoke}. Vlastnosti vsetkych subtried su:
 * <ul>
 * <li>Objekt sa nachadza na nejakom mieste v krajine.</li>
 * <li>Objekt moze byt zniceny.</li>
 * <li>Pocas tahu sa v krajine tieto objekty nenachadzaju, iba medzi tahmi.</li>
 * <li>Pri zniceni posledneho objektu AbstractWeapon sa dostava na tah dalsi hrac.</li>
 * </ul>
 * Kazda zbran musi implementovat metodu {@link #move()} , ktora je pravidelne volana vzdy pred vykreslenim zbrane. Tato
 * metoda opisuje chovanie sa zbrane, ako napriklad pohyb, vytvaranie dalsich zbrani a podobne.
 */
public abstract class AbstractWeapon implements UnselectablePicture {

	/** Objekt na zistovanie vysky krajiny. */
	protected static Land.LandMeter landMeter;

	/** Pozicia zbrane v krajine */
	protected double x;
	/** Pozicia zbrane v krajine */
	protected double y;
	/** Pozicia zbrane v krajine */
	protected double z;

	/**
	 * <code>true</code> ak tato zbran existuje, <code>false</code>, ak tuto zbran treba vyhodit zo zoznamu existujucich
	 * zbrani
	 * <p>
	 * Nastavenie tohto pola na hodnotu <code>false</code> znici zbran pri dalsom prechode cez zoznam zbrani. Zbrane
	 * vyuzivaju tuto vlastnost a mozu zabranit svojmu zniceniu, napriklad za ucelom vytvorenia viacnasobnej explozie.
	 */
	protected boolean exists = true;

	/** Spolocny zoznam zbrani, v ktorom sa nachadzaju vsetky existujuce zbrane. */
	private static WeaponList weaponList;

	/**
	 * Vytvori novu zbran na danom mieste v krajine a zaradi ju do zoznamu existujucich zbrani.
	 *
	 * @param x x-suradnica v krajine
	 * @param y y-suradnica v krajine
	 * @param z z-suradnica v krajine
	 */
	protected AbstractWeapon(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;

		weaponList.add(this);
	}

	/**
	 * Vykona jeden krok danej zbrane. Zbran sa nemusi hybat, ale <b>mala by vykonat vsetky vypocty okrem samotneho
	 * nakreslenia.</b> Tato metoda je volana pravidelne vzdy pred samotnym vykreslenim.
	 */
	abstract public void move();

	/** Vrati, ci je zbran znicena. */
	public boolean isDestroyed() {
		return !exists;
	}

	/**
	 * Vytvori exploziu s epicentrom zhodnym s poziciou tejto zbrane, a znici tuto zbran. Zbran si po zavolani tejto
	 * metody moze opat nastavit pole <code>exists</code> na hodnotu <code>true</code> a tym zabranit zniceniu.
	 *
	 * @param radius polomer vybuchu
	 * @param damage zranenia sposobene v centre vybuchu
	 */
	protected void explode(double radius, double damage) {
		new Explosion(x, y, z, radius, damage);
		exists = false;
	}

	/**
	 * Zrani okolite tanky, ale nevytvori exploziu na efekt ani krater. Zbran nie je znicena.
	 *
	 * @param radius maximalny dosah na sposobenie skod
	 * @param damage maximalne sposobene zranenia
	 */
	protected void damage(double radius, double damage) {
		Explosion.damage(x, y, z, radius, damage);
	}

	public static void setLandMeter(Land.LandMeter landMeter) {
		AbstractWeapon.landMeter = landMeter;
	}

	public static void setWeaponList(WeaponList weaponList) {
		AbstractWeapon.weaponList = weaponList;
	}

	/** Vrati nahodne realne cislo v rozsahu -range az range. */
	protected static double randomRange(double range) {
		return 2 * (Math.random() - 0.5) * range;
	}

}
