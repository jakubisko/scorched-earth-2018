package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.picture.Model;
import sk.tekel04.scorched.picture.SphereDrawer;

/** Raketa, ktora v pravidelnych intervaloch vypusta ohnostroj. Samotna raketa sposobuje len maly vybuch. */
public class MissileShankiFireworks extends AbstractMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("Shanki 666-W<br>Fireworks",
				"Shot periodically releasing massive fireworks; shoot over the target",
				"/icons/MissileShankiFireworks.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileShankiFireworks(x, y, z, direction, angle, power);
			}
		};
	}

	/** Casovy interval medzi jednotlivymi ohnostrojmi. */
	private static final double TIMER_MAX = 18;

	private static Model model;

	/** Pocitadlo casu. Vzdy po dosiahnuti 0 vypusti ohnostroj a pocita odznovu. */
	private double timeToExplode = TIMER_MAX;

	private MissileShankiFireworks(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	/**
	 * Posunie tuto zbran. Na zbran posobi gravitacia. Detekuje kolizie. Ak zbran nema koliziu, posunie pocitadlo casu.
	 * Po dosiahnuti 0 vypusti ohnostroj a pocitadlo opat nastavi na odpocitavanie. Ohnostroj sa sklada zo 150 castic
	 * letiacich na vsetky smery rovnakou rychlostou.
	 */
	@Override
	public void move() {
		super.move();

		timeToExplode -= Game.TIME;
		if (exists && timeToExplode <= 0) {
			for (int i = 0; i < 150; i++) {
				new LittleShankiFireworks(x, y, z, randomRange(180), randomRange(90), 0.5);
			}

			timeToExplode += TIMER_MAX;
		}
	}

	@Override
	protected void collisionEarth() {
		explode(1.6, 72);
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileLarge");
	}

}

/**
 * Jedna cast ohnostroju. Tvori ju farebna gula pohybujuca sa rovnomernou rychlostou smerom prec od epicentra. Pri
 * naraze do kopca exploduje. Ak do kratkeho casoveho limitu nezasiahne prekazku, rozplynie sa bez vybuchu.
 */
class LittleShankiFireworks extends AbstractMovableWeapon {

	/** Farba zbrane. */
	private final double[] color = new double[3];

	/** Pocitadlo casu odratavajuce znicenie zbrane. */
	private double timeToLive;

	/**
	 * Vytvori novu instanciu zbrane, nastavi jej poziciu a rychlost pohybu. Nastavi pocitadlo casu na nahodnu dobu.
	 * Nastavi nahodnu farbu pre tuto cast ohnostroju.
	 */
	public LittleShankiFireworks(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);

		timeToLive = 11 + 11 * Math.random();

		color[0] = Math.random();
		color[1] = Math.random();
		color[2] = Math.random();
	}

	/** Posunie zbran a znizi pocitadlo casu. Po vyprsani zivotnosti zbran zanika bez vybuchu. */
	@Override
	public void move() {
		super.move();

		timeToLive -= Game.TIME;
		if (timeToLive <= 0) {
			exists = false;
		}
	}

	@Override
	protected void collisionEarth() {
		explode(1.4, 36);
	}

	@Override
	public void draw(GL2 gl) {
		if (exists) {
			SphereDrawer.drawSphere(gl, x, y, z, 0.1, 4, color);
		}
	}

}
