package sk.tekel04.scorched.weapon;

import sk.tekel04.scorched.game.Game;

/** Zbran, ktora sa pohybuje. Pamata si svoju rychlost pohybu. Detekuje kolizie. */
public abstract class AbstractMovableWeapon extends AbstractWeapon {

	/** Tolerovana odchylka pri vypocte miesta kolizie. */
	private static final double TOLERANCE = 0.005;

	/** Jedna zlozka rychlosti pohybu zbrane. */
	protected double speedX;
	/** Jedna zlozka rychlosti pohybu zbrane. */
	protected double speedY;
	/** Jedna zlozka rychlosti pohybu zbrane. */
	protected double speedZ;

	/**
	 * Vytvori zbran a nastavi jej poziciu a rychlost pohybu. Rychlost je dana dlzkou vektora a dvomi uhlami, tato
	 * rychlost sa prepocita na jednotlive zlozky v smere kazdej osi.
	 *
	 * @param x x-suradnica zbrane
	 * @param y y-suradnica zbrane
	 * @param z z-suradnica zbrane
	 * @param direction otocnenie vo vodorovnej rovine v stupnoch
	 * @param angle uhol zovrety s vodorovnou rovinou v stupnoch (90 = hore, -90 = dolu)
	 * @param power rychlost pohybu zbrane
	 */
	protected AbstractMovableWeapon(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z);

		double d = power * Math.cos(angle * Math.PI / 180);

		speedX = d * Math.cos(direction * Math.PI / 180);
		speedY = power * Math.sin(angle * Math.PI / 180);
		speedZ = d * Math.sin(direction * Math.PI / 180);
	}

	/**
	 * Nastane pri kolizii s kopcom. Znici tuto zbran. Bezne zbrane zdedene z tejto triedy tuto metodu prekryju, a na
	 * tomto mieste vytvoria vybuch.
	 */
	protected void collisionEarth() {
		exists = false;
	}

	/**
	 * Nastane pri kolizii s vodnou hladinou. Znici tuto zbran. Bezne zbrane zdedene z tejto triedy tuto metodu
	 * neprekyvaju, pretoze bezne zbrane sa vo vode utopia.
	 */
	protected void collisionWater() {
		exists = false;
	}

	/**
	 * Posunie zbran. Posun je realizovany pricitanim vektora rychlosti k aktualnej pozicii. Ak sa nova pozicia nachadza
	 * v kopci alebo pod vodou, posunie zbran na miesto kolizie. Nasledne sa zavola metoda {@link #collisionEarth()},
	 * alebo {@link #collisionWater()}, podla toho, ci ide o naraz do kopca alebo do vody.
	 * <p>
	 * Zbran sa pohybuje v kazdom kroku skokovo. Kontrola kolizie sa vykonava len na novej pozicii, z coho vyplyva, ze
	 * zbran pohybujuca sa rychlostou X moze teoreticky preletiet cez prekazku mensich rozmerov ako X. Pri beznych
	 * rychlostiach toto spravanie nepredstavuje problem, pri vysokych rychlostiach sa odporuca namiesto toho pouzit
	 * nizsiu rychlost a volat metodu {@link #move()} viacnasobne.
	 */
	@Override
	public void move() {
		if (!exists) {
			return;
		}

		double newX = x + speedX * Game.TIME;
		double newY = y + speedY * Game.TIME;
		double newZ = z + speedZ * Game.TIME;

		if (landMeter.height(newX, newZ) >= newY) {
			findCollisionLocation(x, y, z, newX, newY, newZ);
			if (landMeter.height(x, z) > TOLERANCE) {
				collisionEarth();
			} else {
				collisionWater();
			}
		} else {
			x = newX;
			y = newY;
			z = newZ;
		}
	}

	/**
	 * Vrati sucet absolutnych hodnot 3 realnych cisel.
	 *
	 * @param dx realne cislo
	 * @param dy realne cislo
	 * @param dz realne cislo
	 * @return abs(dx) + abs(dy) + abs(dz)
	 */
	private double abs3(double dx, double dy, double dz) {
		return Math.abs(dx) + Math.abs(dy) + Math.abs(dz);
	}

	/**
	 * Posunie zbran na bod prieniku drahy s krajinou. Algoritmus predpoklada, ze povodna pozicia zabrane bola mimo
	 * prekazku, a nova pozicia sa nachadza v prekazke. Dalej sa predpoklada, ze existuje na drahe bod kolizie
	 * nasledovnych vlastnosti:
	 * <li>Od povodneho miesta zbrane az do bodu kolizie sa nenachadza prekazka.</li>
	 * <li>Od bodu kolizie sa az do pozicie zbrane v prekazke nachadza prekazka.</li> Algoritmus potom vyuziva binarne
	 * vyhladavanie na najdenie miesta kolizie. Najdene miesto sa nachadza vo vzdialenosti najviac
	 * <code>{@link #TOLERANCE} / 2</code> od realneho miesta kolizie.
	 *
	 * @param x0 x-suradnica zbrane pred koliziou
	 * @param y0 y-suradnica zbrane pred koliziou
	 * @param z0 z-suradnica zbrane pred koliziou
	 * @param x1 x-suradnica zbrane v prekazke
	 * @param y1 y-suradnica zbrane v prekazke
	 * @param z1 z-suradnica zbrane v prekazke
	 */
	private void findCollisionLocation(double x0, double y0, double z0, double x1, double y1, double z1) {
		while (abs3(x0 - x1, y0 - y1, z0 - z1) >= TOLERANCE / 2) {
			x = (x0 + x1) / 2;
			y = (y0 + y1) / 2;
			z = (z0 + z1) / 2;
			if (landMeter.height(x, z) >= y) {
				x1 = x;
				y1 = y;
				z1 = z;
			} else {
				x0 = x;
				y0 = y;
				z0 = z;
			}
		}
	}

}
