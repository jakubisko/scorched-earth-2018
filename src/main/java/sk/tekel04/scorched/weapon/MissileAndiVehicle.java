package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.Model;

/** Zakladny typ strely - raketa, ktora vybuchne pri naraze do kopca. */
public class MissileAndiVehicle extends AbstractMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("Andi-Vehicle<br>Missile", "Standard projectile",
				"/icons/MissileAndiVehicle.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileAndiVehicle(x, y, z, direction, angle, power);
			}
		};
	}

	private static Model model;

	private MissileAndiVehicle(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	@Override
	protected void collisionEarth() {
		explode(2.4, 158);
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileNormal");
	}

}
