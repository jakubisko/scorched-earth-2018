package sk.tekel04.scorched.weapon;

import sk.tekel04.scorched.game.Game;

/**
 * Zbran, ktora sa pohybuje po drahe balistickej krivky, a ma dlhy dostrel. Gravitacia vsak na ne posobi len polovicnou
 * silou, vdaka comu maju vyssi dostrel. Kedze tieto strely su urcene na zasah vzdialenych cielov, pri ktorych sa tazsie
 * zameriava ciel, pocas letu za sebou zanechavaju dymovu stopu, vdaka ktorej vidno trajektoriu letu. Ak strela padne do
 * vody, zanecha za sebou oblak dymu pre lahsiu identifikaciu miesta dopadu.
 */
public abstract class AbstractHighRangeMissile extends AbstractMissile {

	/** Vytvori novu instanciu zbrane, nastavi jej poziciu a rychlost pohybu. */
	protected AbstractHighRangeMissile(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	/**
	 * Posunie tuto zbran. Na zbran odvodenu z triedy {@link AbstractHighRangeMissile} posobi gravitacia len polovicnou
	 * silou. Pocas pohybu za sebou strela zanechava dymovu stopu na zobrazenie trajektorie. Detekuje kolizie.
	 */
	@Override
	public void move() {
		speedY += GRAVITATION * Game.TIME / 2;
		super.move();

		new Smoke(x, y, z, 0.1);
	}

	/**
	 * Nastane pri kolizii s vodnou hladinou. Znici tuto zbran. Zbrane odvodene z triedy
	 * {@link AbstractHighRangeMissile} pri pade do vody vytvaraju oblak dymu pre lahsiu identifikaciu miesta dopadu.
	 */
	@Override
	protected void collisionWater() {
		for (int i = 0; i < 90; i++) {
			new Smoke(x, y, z, 3);
		}
		super.collisionWater();
	}

}
