package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.Model;

/** Raketa, ktora po dopade exploduje a vytvori dve sustredene kruznice dalsich rakiet. */
public class MissileChiccy extends AbstractMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("Chiccy Flame<br>Bomb XXL",
				"Projectile that divides on impact to a circle of smaller shells", "/icons/MissileChiccy.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileChiccy(x, y, z, direction, angle, power);
			}
		};
	}

	private static Model model;

	private MissileChiccy(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	/** Pri kolizii so zemou exploduje a vytvori dve kruznice mensich rakiet. */
	@Override
	protected void collisionEarth() {
		explode(1.2, 50);

		for (int i = 0; i < 12; i++) {
			new LittleChiccy(x, y, z, 30 * i, 70, 0.35);
			new LittleChiccy(x, y, z, 15 + 30 * i, 85, 0.35);
		}
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileLarge");
		LittleChiccy.loadModel(gl);
	}

}

/** Mala raketa, ktora vznikla po explozii {@link MissileChiccy}. */
class LittleChiccy extends AbstractMissile {

	private static Model model;

	public LittleChiccy(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	@Override
	protected void collisionEarth() {
		explode(1.22, 50);
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	protected static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileNormal");
	}

}
