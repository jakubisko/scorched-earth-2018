package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.picture.Model;
import sk.tekel04.scorched.picture.SphereDrawer;

/**
 * Strela, ktora sa po naraze rozleti na snehove gule, ktore sa este dalej rozdelia na snehove vlocky. Tie padaju ako
 * sneh na zem. Tato zbran nevytvara krater. Pocet mensich castic je priamo umerny casu, ktory trval let. Tato zbran je
 * preto silnejsia na velke vzdialenosti.
 */
public class MissileFlurryFreezer extends AbstractHighRangeMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("High-Range<br>Flurry-Freezer",
				"Missile creating snow storm; effectiveness increases with distance",
				"/icons/MissileFlurryFreezer.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileFlurryFreezer(x, y, z, direction, angle, power);
			}
		};
	}

	private static Model model;

	/** Pocitadlo dlzky letu. */
	private double timeFlying;

	private MissileFlurryFreezer(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	/** Posunie zbran a zvysi pocitadlo dlzky letu o 1. */
	@Override
	public void move() {
		super.move();
		timeFlying += Game.TIME;
	}

	/**
	 * Pri naraze do zeme vznikne pocet mensich castic priamo umerny dlzke letu. Tieto castice sa rozletia nahodnym
	 * smerom rovnakou rychlostou do hornej polroviny krajiny.
	 */
	@Override
	protected void collisionEarth() {
		for (int i = 0; i < timeFlying + 10; i++) {
			new LittleFlurryFreezer(x, y, z, randomRange(180), 45 + randomRange(45), 0.5);
		}
		super.collisionEarth();
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileLong");
	}
}

/**
 * Jeden ulomok zbrane {@link MissileFlurryFreezer}, predstavuje snehovu gulu letiacu prec od miesta dopadu. Po kratkom
 * case sa rozpadne na snehove vlocky, ktore padaju na zem. Ak objekt tejto triedy narazi do prekazky pred tym, ako
 * uplynie cas potrebny na uvolnenie vlociek, zmizne bez vedlajsieho efektu.
 */
class LittleFlurryFreezer extends AbstractMovableWeapon {

	/** Farba snehovej gule. */
	private double[] color = new double[3];

	/** Pocitadlo casu, po dosiahnuti 0 sa rozpadne na snehove vlocky. */
	private double timeToLive;

	/**
	 * Vytvori zbran, nastavi jej poziciu, smer a rychlost pohybu. Nastavi aj pocitadlo casu, po vyprsani casoveho
	 * limitu sa zbran dalej rozpada. Nastavi nahodny odtien svetlomodrej ako farbu tejto snehovej gule.
	 */
	public LittleFlurryFreezer(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);

		timeToLive = 10 + 12 * Math.random();

		color[0] = Math.random();
		color[1] = 1;
		color[2] = 1;
	}

	/** Posunie zbran a znizi pocitadlo casu. Po dosiahnuti 0 zbran zanika a rozpada sa na snehove vlocky. */
	@Override
	public void move() {
		super.move();

		timeToLive -= Game.TIME;
		if (exists && timeToLive <= 0) {
			for (int i = 0; i < 8; i++) {
				new FallingFlurryFreezer(x, y, z);
			}
			exists = false;
		}
	}

	@Override
	public void draw(GL2 gl) {
		if (exists) {
			SphereDrawer.drawSphere(gl, x, y, z, 0.12, 8, color);
		}
	}
}

/**
 * Objekt triedy FallingFlurryFreezer reprezentuje jednu snehovu vlocku. Vlocka pada k zemi nepravidelnym pohybom. Po
 * dopade na zem alebo na vodu zrani okolite tanky. Nevyvolava exploziu.
 */
class FallingFlurryFreezer extends AbstractMovableWeapon {

	/** Farba snehovej vlocky. */
	private double[] color = new double[3];

	/**
	 * Vytvori zbran, nastavi jej poziciu, smer a rychlost pohybu. Nastavi nahodny odtien svetlomodrej ako farbu tejto
	 * snehovej vlocky.
	 */
	public FallingFlurryFreezer(double x, double y, double z) {
		super(x + randomRange(0.6), y + randomRange(0.6), z + randomRange(0.6), 0, -90, 0.2);

		color[0] = Math.random();
		color[1] = 1;
		color[2] = 1;
	}

	@Override
	protected void collisionEarth() {
		damage(1.6, 13);
		super.collisionEarth();
	}

	@Override
	protected void collisionWater() {
		damage(1.6, 13);
		super.collisionWater();
	}

	/** Posunie zbran. Vlocka pada nepravidelnym pohybom smerom nadol. */
	@Override
	public void move() {
		speedX = randomRange(0.22);
		speedZ = randomRange(0.22);

		super.move();
	}

	@Override
	public void draw(GL2 gl) {
		if (exists) {
			SphereDrawer.drawSphere(gl, x, y, z, 0.08, 8, color);
		}
	}

}
