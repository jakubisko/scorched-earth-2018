package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.picture.Model;
import sk.tekel04.scorched.picture.SphereDrawer;

/** Bomba, ktora sa po naraze rozleti na vela malych ohnivych bomb. Hori aj na vode. */
public class MissileHolotovCoctail extends AbstractMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("Holotov Coctail", "Explosive shell that combusts even on water",
				"/icons/MissileHolotovCoctail.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileHolotovCoctail(x, y, z, direction, angle, power);
			}
		};
	}

	private static Model model;

	private MissileHolotovCoctail(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	/** Pri naraze do zeme sa rozleti na 50 nahodne nasmerovanych mensich bomb. */
	@Override
	protected void collisionEarth() {
		for (int i = 0; i < 50; i++) {
			new LittleHolotovCoctail(x, y + 0.02, z, randomRange(180), 65 + randomRange(25), 0.3 + randomRange(0.2));
		}
		super.collisionEarth();
	}

	/** Pri dopade do vody sa sprava rovnako ako pri dopade na zem. */
	@Override
	protected void collisionWater() {
		collisionEarth();
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileLarge");
	}

}

/** Mala ohniva bomba, ktora vznikne po explozii {@link MissileHolotovCoctail}. Hori aj na vode. */
class LittleHolotovCoctail extends AbstractMissile {

	/** Farba malej ohnivej bomby - zlta */
	private static final double[] COLOR = { 1, 1, 0 };

	public LittleHolotovCoctail(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	@Override
	protected void collisionEarth() {
		explode(1.2, 28);
	}

	@Override
	protected void collisionWater() {
		explode(1.2, 28);
	}

	@Override
	public void draw(GL2 gl) {
		if (exists) {
			SphereDrawer.drawSphere(gl, x, y, z, 0.08, 8, COLOR);
		}
	}

}
