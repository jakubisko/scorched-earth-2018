package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.logging.Log;
import sk.tekel04.scorched.picture.Model;

/** Zbran, ktora sa pohybuje po drahe balistickej krivky. Detekuje kolizie. */
public abstract class AbstractMissile extends AbstractMovableWeapon {

	/** Gravitacne zrychlenie. */
	protected static final double GRAVITATION = 0.02;

	/** Vytvori novu instanciu zbrane, nastavi jej poziciu a rychlost pohybu. */
	protected AbstractMissile(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	/** Posunie tuto zbran. Na balisticke strely posobi gravitacia. Detekuje kolizie. */
	@Override
	public void move() {
		speedY -= GRAVITATION * Game.TIME;

		super.move();
	}

	/**
	 * Nakresli {@link Model} zbrane. Vacsina zbrani pohybujucich sa po balistickej krivke sa kresli v podobe rakety.
	 * Tato metoda nakresli tento model, prislusne otoceny podla smerovania zbrane. Zbrane, ktore sa pohybuju po
	 * balistickej krivke a nie su reprezentovane modelom, tuto metodu nevyuzivaju.
	 *
	 * @param select <code>true</code>, ak objekt ma navyse udavat aj svoj identifikator, aby bolo mozne urcit, ktory
	 *            objekt si pouzivatel vybral mysou
	 * @param model model zbrane, ktory sa ma nakreslit
	 */
	public void draw(GL2 gl, Model model) {
		if (!exists) {
			return;
		}

		gl.glPushMatrix();

		double d = Math.sqrt(speedX * speedX + speedZ * speedZ);

		gl.glTranslated(x, y, z);
		gl.glRotated(Math.atan2(speedZ, speedX) * 180 / Math.PI, 0, -1, 0);
		gl.glRotated(Math.atan2(speedY, d) * 180 / Math.PI, 0, 0, 1);

		model.draw(gl);

		gl.glPopMatrix();
	}

	/** Nacita modely vsetkych zbrani. */
	public static void loadModels(GL2 gl) {
		Log.entering("AbstractMissile", "loadTextures");

		MissileAndiVehicle.loadModel(gl);
		MissileWaterBounceFarmer.loadModel(gl);
		MissileChiccy.loadModel(gl);
		MissileHolotovCoctail.loadModel(gl);
		MissileShankiFireworks.loadModel(gl);
		MissileMultiMaros.loadModel(gl);
		MissileFlurryFreezer.loadModel(gl);
		MissileNoxious.loadModel(gl);
		MissileLuVar.loadModel(gl);
		MissileMapa.loadModel(gl);

		Log.exiting("AbstractMissile", "loadTextures");
	}

}
