package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

/**
 * Strela s obrovskym vybuchom, ale sposobuje iba minimalne zranenia. Oproti ostatnym strelam je vynimocna tym, ze je
 * neviditelna.
 */
public class Missile3on extends AbstractMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("Secret 3on's<br>Invisible Weapon",
				"Invisible shell with very large explosion dealing minimal damage", "/icons/Missile3on.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new Missile3on(x, y, z, direction, angle, power);
			}
		};
	}

	private Missile3on(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	@Override
	protected void collisionEarth() {
		explode(6.2, 5.5);
	}

	/** Nakresli strelu. */
	@Override
	public void draw(GL2 gl) {
		// Missile3on je neviditelna.
	}

}
