package sk.tekel04.scorched.weapon;

import java.util.List;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.game.Tank;
import sk.tekel04.scorched.picture.SphereDrawer;

/**
 * Ohniva gula vybuchu. Zrani okolite tanky a vytvori krater. Obsahuje staticku metodu damage, ktora zrani okolite tanky
 * bez toho, aby sposobila vybuch alebo vytvorila krater. Niektore zbrane zranuju aj bez doprovodnej ohnivej gule.
 */
public class Explosion extends AbstractWeapon {

	/** Farba vybuchu - cervena */
	private static final double[] color = { 1, 0, 0 };

	/** Zoznam tankov v hre. */
	private static List<Tank> tanky;

	/** Polomer vybuchu. */
	private final double radius;

	/** Zranenia sposobene vybuchom v epicentre. */
	private final double damage;

	/**
	 * Vytvori novy vybuch na danom mieste krajiny.
	 *
	 * @param x x-suradnica epicentra
	 * @param y y-suradnica epicentra
	 * @param z z-suradnica epicentra
	 * @param radius polomer vybuchu
	 * @param damage zranenia sposobene v epicentre
	 */
	public Explosion(double x, double y, double z, double radius, double damage) {
		super(x, y, z);

		this.radius = radius;
		this.damage = damage;
	}

	/**
	 * Zrani okolite tanky bez grafickeho efektu a vytvorenia krateru.
	 *
	 * @param x x-suradnica epicentra
	 * @param y y-suradnica epicentra
	 * @param z z-suradnica epicentra
	 * @param polomer maximalny dosah na sposobenie skod
	 * @param damage zranenia sposobene v epicentre
	 */
	public static void damage(double x, double y, double z, double polomer, double damage) {
		for (int i = 0; i < tanky.size(); i++) {
			Tank tank = tanky.get(i);
			if (tank != null) {
				tank.damage(x, y, z, polomer, damage);
			}
		}
	}

	/** Zrani okolite tanky a vytvori oblaciky dymu v okoli vybuchu. */
	@Override
	public void move() {
		damage(x, y, z, radius, damage);

		for (int i = (int) (3 * radius * radius * radius); i >= 0; i--) {
			new Smoke(x, y, z, radius);
		}
	}

	/**
	 * Nakresli ohnivu gulu vybuchu a vytvori krater v krajine. Tato metoda tvori vynimku medzi zbranami, pretoze
	 * obsahuje vypocet nesuvisiaci s vykreslenim. V tomto pripade je to ale mozne, pretoze na konci kreslenia sa tato
	 * zbran znici, a teda sa nemoze stat, ze by bola nakreslena viac krat.
	 */
	@Override
	public void draw(GL2 gl) {
		if (!exists) {
			return;
		}

		SphereDrawer.drawSphere(gl, x, y, z, radius, 16, color);

		landMeter.makeCrater(x, y, z, radius);
		exists = false;
	}

	/**
	 * Nastavi zoznam tankov.
	 *
	 * @param tanky zoznam tankov v hre
	 * @uml.property name="tanky"
	 */
	public static void setTanky(List<Tank> tanky) {
		Explosion.tanky = tanky;
	}

}
