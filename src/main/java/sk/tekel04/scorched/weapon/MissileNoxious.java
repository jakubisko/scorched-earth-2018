package sk.tekel04.scorched.weapon;

import com.jogamp.opengl.GL2;

import sk.tekel04.scorched.game.Game;
import sk.tekel04.scorched.picture.Model;
import sk.tekel04.scorched.picture.SphereDrawer;

/**
 * Raketa, ktora vypusta jedovaty plyn. Nesposobuje ziadnu exploziu. Aktivuje sa po vyprsani casoveho limitu, takze
 * nezrani tank, ktory ju vystrelil.
 */
public class MissileNoxious extends AbstractMissile {

	public static UserMissileFactory getFactory() {
		return new UserMissileFactory("High-Range<br>Noxious Bio Dart",
				"High-range missile releasing corrosive material; shoot over the target", "/icons/MissileNoxious.png") {
			@Override
			public void createInstance(double x, double y, double z, double direction, double angle, double power) {
				new MissileNoxious(x, y, z, direction, angle, power);
			}
		};
	}

	private static Model model;

	/** Pocitadlo casoveho limitu na aktivovanie vypustania jedu. */
	private double timeBeforeRelease = 8;

	private MissileNoxious(double x, double y, double z, double direction, double angle, double power) {
		super(x, y, z, direction, angle, power);
	}

	/** Posunie zbran. Znizi pocitadlo casoveho limitu, ak cas vyprsal, vypusti jed. */
	@Override
	public void move() {
		super.move();

		int timerBefore = (int) Math.floor(timeBeforeRelease);
		timeBeforeRelease -= Game.TIME;
		int timerAfter = (int) Math.floor(timeBeforeRelease);

		if (exists && timerBefore <= 0) {
			for (int i = 0; i < timerBefore - timerAfter; i++) {
				new LittleNoxious(x, y, z);
			}
		}
	}

	@Override
	public void draw(GL2 gl) {
		super.draw(gl, model);
	}

	public static void loadModel(GL2 gl) {
		model = new Model(gl, "missile.png", "MissileLong");
	}

}

/**
 * Vypusti oblacik jedovateho dymu. Nesposobuje vybuch, ale ubera zivoty okolitym tankom. Pohybuje sa nahodnym smerom,
 * pada k zemi.
 */
class LittleNoxious extends AbstractMissile {

	/** Farba jedovateho plynu - zelena. */
	private static final double[] COLOR = { 0, 0.8, 0 };

	/**
	 * Vytvori oblak jedovateho plynu na danom mieste. Oblak sa zacne hybat nahodnym smerok k zemi.
	 *
	 * @param x x-suradnica v krajine
	 * @param y y-suradnica v krajine
	 * @param z z-suradnica v krajine
	 */
	public LittleNoxious(double x, double y, double z) {
		super(x, y, z, 0, 0, 0);

		speedX = randomRange(0.1);
		speedY = randomRange(0.3);
		speedZ = randomRange(0.1);
	}

	/**
	 * Posunie jedovaty oblak. Na oblak posobi gravitacia, pohybuje sa po balistickej krivke. Pocas pohybu poskodzuje
	 * vsetky okolite tanky.
	 */
	@Override
	public void move() {
		super.move();

		if (exists) {
			damage(1.7, 21 * Game.TIME);
		}
	}

	@Override
	public void draw(GL2 gl) {
		if (exists) {
			SphereDrawer.drawSphere(gl, x, y, z, 0.1, 8, COLOR);
		}
	}

}
