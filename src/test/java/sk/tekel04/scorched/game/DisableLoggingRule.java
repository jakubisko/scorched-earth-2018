package sk.tekel04.scorched.game;

import java.util.logging.LogManager;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/** Add instance of this class annotated with @Rule in your unit test to disable all logging during the test. */
public class DisableLoggingRule extends TestWatcher {

	@Override
	public void starting(Description desc) {
		LogManager.getLogManager().reset();
	}

}
