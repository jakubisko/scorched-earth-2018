package sk.tekel04.scorched.game;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

public class TankTest {

	@Rule
	public TestRule disableLogging = new DisableLoggingRule();

	/** Pozicia testovacieho tanku v krajine. */
	private static final int TANK_X = 2;

	/** Pozicia testovacieho tanku v krajine. */
	private static final int TANK_Y = 2;

	/** Zranenia sposobene testovacim vybuchom. */
	private static final int DAMAGE = 10;

	private Land land = new Land();
	private Land.LandMeter landMeter = land.new LandMeter();
	private Tank tank;
	private double TANK_Z;

	/** Nacita krajinu zo suboru, ktoreho obsah pozname. Vytvori na vrchole kopca jeden tank, ktory budeme testovat. */
	@Before
	public void setUp() throws URISyntaxException, IOException {
		// Nacitanie testovacej krajiny.
		URI uri = getClass().getClassLoader().getResource("test_land.txt").toURI();
		Path testLandFile = Paths.get(uri);
		land.loadFromFile(testLandFile);

		// Vytvori testovany tank.
		tank = new Tank(0, 0, TANK_X, TANK_Y);
		Tank.setLandMeter(landMeter);

		// Vyska krajiny v bode, kde stoji tank.
		TANK_Z = landMeter.height(TANK_X, TANK_Y);
	}

	/* Vybuch, ktory je dalej ako jeho polomer, nema sposobit stratu zivotov. */
	@Test
	public void damageTooFarTest() {
		double life_before = tank.getLife();
		tank.damage(TANK_X, TANK_Z + 15, TANK_Y, 12, DAMAGE);
		double dealt = life_before - tank.getLife();
		Assert.assertEquals(0, dealt, 0.01);
	}

	/* Vybuch, ktory by sposobil zaporne vela zraneni, nerobi nic. */
	@Test
	public void damageNegativeTest() {
		double life_before = tank.getLife();
		tank.damage(TANK_X, TANK_Z, TANK_Y, 12, -DAMAGE);
		double dealt = life_before - tank.getLife();
		Assert.assertEquals(0, dealt, 0.01);
	}

	/* Vybuch, ktory zasiahne presne, sposobi prave dany pocet zraneni. */
	@Test
	public void damageDirectHitTest() {
		double life_before = tank.getLife();
		tank.damage(TANK_X + 0.5, TANK_Z, TANK_Y + 0.5, 12, DAMAGE);
		double dealt = life_before - tank.getLife();
		Assert.assertEquals(DAMAGE, dealt, 0.01);
	}

	/*
	 * Zranenia sposobene vybuchom musia klesat so vzrastajucou vzdialenostou. Pokial je vzdialenost mensia ako polomer,
	 * musi vybuch sposobit viac ako 0 zraneni.
	 */
	@Test
	public void damageDecreasesTest() {
		// Kolko zraneni dal predosly vybuch
		double dealt_before = DAMAGE;

		for (double distance = 1; distance < 12; distance++) {
			double life_before = tank.getLife();
			tank.damage(TANK_X + 0.5, TANK_Z + distance, TANK_Y + 0.5, 12, DAMAGE);
			double dealt = life_before - tank.getLife();

			Assert.assertTrue(dealt > 0);
			Assert.assertTrue(dealt < dealt_before);

			dealt_before = dealt;
		}
	}

}
