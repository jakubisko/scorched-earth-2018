package sk.tekel04.scorched.game;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

public class LandMeterTest {

	@Rule
	public TestRule disableLogging = new DisableLoggingRule();

	private Land land = new Land();
	private Land.LandMeter landMeter = land.new LandMeter();

	/** Nacita testovaciu krajinu zo suboru. */
	@Before
	public void setUp() throws URISyntaxException, IOException {
		URI uri = getClass().getClassLoader().getResource("test_land.txt").toURI();
		Path testLandFile = Paths.get(uri);
		land.loadFromFile(testLandFile);
	}

	/** Vyska krajiny na vrchole. */
	@Test
	public void heightTest1() {
		Assert.assertEquals(1.0, landMeter.height(3, 3), 0.01);
	}

	/** Vyska krajiny na hrane. */
	@Test
	public void heightTest2() {
		Assert.assertEquals(1.0, landMeter.height(3, 2.75), 0.01);
	}

	/** Vyska krajiny uprostred steny. */
	@Test
	public void heightTest3() {
		Assert.assertEquals(1.5, landMeter.height(2, 2.75), 0.01);
	}

	/** Vyska mimo krajinu - 0, lebo donekonecna je voda. */
	@Test
	public void heightTest4() {
		Assert.assertEquals(0.0, landMeter.height(17, 1), 0.01);
	}

	/** Policko leziace za koncom krajiny. */
	@Test
	public void isWaterTest1() {
		Assert.assertTrue(landMeter.isWater(96, 13));
	}

	/** Cele policko pod vodou. */
	@Test
	public void isWaterTest2() {
		Assert.assertTrue(landMeter.isWater(0, 0));
	}

	/** Jeden vrchol policka je pod vodou . */
	@Test
	public void isWaterTest3() {
		Assert.assertTrue(landMeter.isWater(1, 0));
	}

	/** Cele policko je na suchu. */
	@Test
	public void isWaterTest4() {
		Assert.assertFalse(landMeter.isWater(2, 2));
	}

	@Test
	public void makePlatformTest() {
		// Zarovname policko.
		double h = landMeter.makePlatform(2, 2);

		// Overime, ci je zarovnane na dobru vysku.
		Assert.assertEquals(h, (4 * 2 + 5 * 1) / 9.0, 0.01);

		// Kazdy bod zarovnaneho policka by mal mat rovnaku vysku.
		for (double j = 2; j <= 3; j += 0.1) {
			for (double i = 2; i <= 3; i += 0.1) {
				Assert.assertEquals(h, landMeter.height(i, j), 0.01);
			}
		}
	}

	/* Vybuch na vode nezmeni vysku bodu v krajine, lebo nizsie sa uz ist neda. */
	@Test
	public void makeCraterOnWaterTest() {
		double before = landMeter.height(10, 0);
		landMeter.makeCrater(10, 0, 0, 1);
		double after = landMeter.height(10, 0);
		Assert.assertEquals(before, after, 0.01);
	}

	/* Vybuch vyssie ako krajina nezmeni vysku, lebo nezasiahne zem. */
	@Test
	public void makeCraterTooHighTest() {
		double before = landMeter.height(3, 3);
		landMeter.makeCrater(3, 15, 3, 8);
		double after = landMeter.height(3, 3);
		Assert.assertEquals(before, after, 0.01);
	}

	/* Vybuch, ktory zasiahne krajinu, znizi jej vysku. */
	@Test
	public void makeCraterHitsLandTest() {
		double before = landMeter.height(2, 2);
		landMeter.makeCrater(2, 2, 2, 2);
		double after = landMeter.height(2, 2);
		Assert.assertTrue(before > after);
	}

}
